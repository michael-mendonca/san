import 'package:SAN/models/alimento.dart';
import 'package:SAN/models/receita.dart';

class Refeicao {
  int id;
  //int categoriaId;
  String createdAt;
  String updatedAt;
  List<Alimento> alimentos;
  List<Receita> receitas;
  Categoria categoria;

  Refeicao.cafe()
      : this(categoria: Categoria(id: 1, nome: 'Café da manhã'), alimentos: [
          // Alimento(
          //     nome: "Suco",
          //     detalhe: "Natural, de polpa",
          //     quantidade: 300,
          //     kcal: 100,
          //     unidade: Unidade(nome: 'ml'),
          //     pivot: Pivot(porcoes: 2))
        ], receitas: []);
  Refeicao.almoco()
      : this(
            categoria: Categoria(id: 2, nome: 'Almoço'),
            alimentos: [],
            receitas: []);
  Refeicao.jantar()
      : this(
            categoria: Categoria(id: 3, nome: 'Jantar'),
            alimentos: [],
            receitas: []);
  Refeicao.lanche()
      : this(
            categoria: Categoria(id: 4, nome: 'Lanche'),
            alimentos: [],
            receitas: []);
  Refeicao.ceia()
      : this(
            categoria: Categoria(id: 5, nome: 'Ceia'),
            alimentos: [],
            receitas: []);

  Refeicao({this.id, this.alimentos, this.receitas, this.categoria});

  Refeicao.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['alimentos'] != null) {
      alimentos = new List<Alimento>();
      json['alimentos'].forEach((v) {
        alimentos.add(new Alimento.fromJson(v));
      });
    }
    if (json['receitas'] != null) {
      receitas = new List<Receita>();
      json['receitas'].forEach((v) {
        receitas.add(new Receita.fromJson(v));
      });
    }
    categoria = json['categoria'] != null
        ? new Categoria.fromJson(json['categoria'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.alimentos != null) {
      data['alimentos'] = this.alimentos.map((v) => v.toJson()).toList();
    }
    if (this.receitas != null) {
      data['receitas'] = this.receitas.map((v) => v.toJson()).toList();
    }
    if (this.categoria != null) {
      data['categoria'] = this.categoria.toJson();
    }
    return data;
  }
}

class Categoria {
  int id;
  String nome;

  Categoria({this.id, this.nome});

  Categoria.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    return data;
  }
}
