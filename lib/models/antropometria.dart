import 'package:SAN/models/paciente.dart';

class Antropometria {
  int id;
  String genero;
  int peso;
  int altura;
  int pct;
  int pcb;
  int pcse;
  int pcpeitoral;
  int pcab;
  int pcpcsi;
  int pccoxa;
  int pcpant;
  int cbraco;
  int ccintura;
  int cabdominal;
  int cquadril;
  int ccoxa;
  int cpanturrilha;
  double compleicao;
  double imc;
  double gordura;
  double massamagra;
  double pesoideal;
  String createdAt;
  String updatedAt;
  Paciente paciente;

  Antropometria.empty(
      {this.genero = 'masc',
      this.peso = 30,
      this.altura = 100,
      this.imc = 0,
      this.pct = 0,
      this.pcb = 0,
      this.pcse = 0,
      this.pcpeitoral = 0,
      this.pcab = 0,
      this.pcpcsi = 0,
      this.pccoxa = 0,
      this.pcpant = 0,
      this.cbraco = 0,
      this.ccintura = 0,
      this.cabdominal = 0,
      this.cquadril = 0,
      this.ccoxa = 0,
      this.cpanturrilha = 0,
      this.compleicao = 0,
      this.gordura = 0,
      this.massamagra = 0,
      this.pesoideal = 0});

  Antropometria(
      {this.id,
      this.genero,
      this.peso,
      this.altura,
      this.imc,
      this.pct,
      this.pcb,
      this.pcse,
      this.pcpeitoral,
      this.pcab,
      this.pcpcsi,
      this.pccoxa,
      this.pcpant,
      this.cbraco,
      this.ccintura,
      this.cabdominal,
      this.cquadril,
      this.ccoxa,
      this.cpanturrilha,
      this.compleicao,
      this.gordura,
      this.massamagra,
      this.pesoideal,
      this.createdAt,
      this.updatedAt,
      this.paciente});

  Antropometria.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    genero = json['genero'];
    peso = json['peso'];
    altura = json['altura'];
    imc = json['imc'].toDouble();
    pct = json['pct'];
    pcb = json['pcb'];
    pcse = json['pcse'];
    pcpeitoral = json['pcpeitoral'];
    pcab = json['pcab'];
    pcpcsi = json['pcpcsi'];
    pccoxa = json['pccoxa'];
    pcpant = json['pcpant'];
    cbraco = json['cbraco'];
    ccintura = json['ccintura'];
    cabdominal = json['cabdominal'];
    cquadril = json['cquadril'];
    ccoxa = json['ccoxa'];
    cpanturrilha = json['cpanturrilha'];
    compleicao = json['compleicao'].toDouble();
    gordura = json['gordura'].toDouble();
    massamagra = json['massamagra'].toDouble();
    pesoideal = json['pesoideal'].toDouble();
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    paciente = json['paciente'] != null
        ? new Paciente.fromJson(json['paciente'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['genero'] = this.genero;
    data['peso'] = this.peso;
    data['altura'] = this.altura;
    data['imc'] = this.imc;
    data['pct'] = this.pct;
    data['pcb'] = this.pcb;
    data['pcse'] = this.pcse;
    data['pcpeitoral'] = this.pcpeitoral;
    data['pcab'] = this.pcab;
    data['pcpcsi'] = this.pcpcsi;
    data['pccoxa'] = this.pccoxa;
    data['pcpant'] = this.pcpant;
    data['cbraco'] = this.cbraco;
    data['ccintura'] = this.ccintura;
    data['cabdominal'] = this.cabdominal;
    data['cquadril'] = this.cquadril;
    data['ccoxa'] = this.ccoxa;
    data['cpanturrilha'] = this.cpanturrilha;
    data['compleicao'] = this.compleicao;
    data['gordura'] = this.gordura;
    data['massamagra'] = this.massamagra;
    data['pesoideal'] = this.pesoideal;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.paciente != null) {
      data['paciente'] = this.paciente.toJson();
    }
    return data;
  }
}
