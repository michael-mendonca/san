class Alimento {
  int id;
  String nome;
  String detalhe;
  int quantidade;
  Unidade unidade;
  num kcal;
  num proteina;
  num lipideos;
  num colesterol;
  num carboidratos;
  num fibra;
  num cinzas;
  num calcio;
  num magnesio;
  num manganes;
  num fosforo;
  num ferro;
  num sodio;
  num potassio;
  num cobre;
  num zinco;
  int usuarioId;
  String createdAt;
  String updatedAt;
  PivotRefeicao pivotRefeicao;
  PivotReceita pivotReceita;

  Alimento(
      {this.id,
      this.nome,
      this.detalhe,
      this.quantidade,
      this.unidade,
      this.kcal,
      this.proteina,
      this.lipideos,
      this.colesterol,
      this.carboidratos,
      this.fibra,
      this.cinzas,
      this.calcio,
      this.magnesio,
      this.manganes,
      this.fosforo,
      this.ferro,
      this.sodio,
      this.potassio,
      this.cobre,
      this.zinco,
      this.usuarioId,
      this.createdAt,
      this.updatedAt,
      this.pivotRefeicao});

  Alimento.empty({
    this.quantidade = 0,
    this.kcal = 0,
    this.proteina = 0,
    this.lipideos = 0,
    this.colesterol = 0,
    this.carboidratos = 0,
    this.fibra = 0,
    this.cinzas = 0,
    this.calcio = 0,
    this.magnesio = 0,
    this.manganes = 0,
    this.fosforo = 0,
    this.ferro = 0,
    this.sodio = 0,
    this.potassio = 0,
    this.cobre = 0,
    this.zinco = 0,
  });

  Alimento.copy(Alimento a) {
    this.id = a.id;
    this.nome = a.nome;
    this.detalhe = a.detalhe;
    this.quantidade = a.quantidade;
    this.unidade = a.unidade;
    this.kcal = a.kcal;
    this.proteina = a.proteina;
    this.lipideos = a.lipideos;
    this.colesterol = a.colesterol;
    this.carboidratos = a.carboidratos;
    this.fibra = a.fibra;
    this.cinzas = a.cinzas;
    this.calcio = a.calcio;
    this.magnesio = a.magnesio;
    this.manganes = a.manganes;
    this.fosforo = a.fosforo;
    this.ferro = a.ferro;
    this.sodio = a.sodio;
    this.potassio = a.potassio;
    this.cobre = a.cobre;
    this.zinco = a.zinco;
    this.usuarioId = a.usuarioId;
    this.createdAt = a.createdAt;
    this.updatedAt = a.updatedAt;
    this.pivotRefeicao = PivotRefeicao(porcoes: 1);
  }

  Alimento.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
    detalhe = json['detalhe'];
    quantidade = json['quantidade'];
    kcal = json['kcal'];
    proteina = json['proteina'];
    lipideos = json['lipideos'];
    colesterol = json['colesterol'];
    carboidratos = json['carboidratos'];
    fibra = json['fibra'];
    cinzas = json['cinzas'];
    calcio = json['calcio'];
    magnesio = json['magnesio'];
    manganes = json['manganes'];
    fosforo = json['fosforo'];
    ferro = json['ferro'];
    sodio = json['sodio'];
    potassio = json['potassio'];
    cobre = json['cobre'];
    zinco = json['zinco'];
    usuarioId = json['usuario_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    unidade =
        json['unidade'] != null ? new Unidade.fromJson(json['unidade']) : null;
    pivotRefeicao = json['pivot'] != null
        ? new PivotRefeicao.fromJson(json['pivot'])
        : null;
    pivotReceita =
        json['pivot'] != null ? new PivotReceita.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    data['detalhe'] = this.detalhe;
    data['quantidade'] = this.quantidade;
    data['unidade'] = this.unidade;
    data['kcal'] = this.kcal;
    data['proteina'] = this.proteina;
    data['lipideos'] = this.lipideos;
    data['colesterol'] = this.colesterol;
    data['carboidratos'] = this.carboidratos;
    data['fibra'] = this.fibra;
    data['cinzas'] = this.cinzas;
    data['calcio'] = this.calcio;
    data['magnesio'] = this.magnesio;
    data['manganes'] = this.manganes;
    data['fosforo'] = this.fosforo;
    data['ferro'] = this.ferro;
    data['sodio'] = this.sodio;
    data['potassio'] = this.potassio;
    data['cobre'] = this.cobre;
    data['zinco'] = this.zinco;
    data['usuario_id'] = this.usuarioId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.unidade != null) {
      data['unidade'] = this.unidade.toJson();
    }
    if (this.pivotRefeicao != null) {
      data['pivot'] = this.pivotRefeicao.toJson();
    }
    if (this.pivotReceita != null) {
      data['pivot'] = this.pivotReceita.toJson();
    }
    return data;
  }
}

class PivotRefeicao {
  int alimentoId;
  int refeicaoId;
  num porcoes;

  PivotRefeicao({this.alimentoId, this.refeicaoId, this.porcoes});

  PivotRefeicao.fromJson(Map<String, dynamic> json) {
    alimentoId = json['alimento_id'];
    refeicaoId = json['refeicao_id'];
    porcoes = json['porcoes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['alimento_id'] = this.alimentoId;
    data['refeicao_id'] = this.refeicaoId;
    data['porcoes'] = this.porcoes;
    return data;
  }
}

class PivotReceita {
  int alimentoId;
  int receitaId;
  num porcoes;

  PivotReceita({this.alimentoId, this.receitaId, this.porcoes});

  PivotReceita.fromJson(Map<String, dynamic> json) {
    alimentoId = json['alimento_id'];
    receitaId = json['receita_id'];
    porcoes = json['porcoes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['alimento_id'] = this.alimentoId;
    data['receita_id'] = this.receitaId;
    data['porcoes'] = this.porcoes;
    return data;
  }
}

class Unidade {
  int id;
  String nome;

  Unidade({this.id, this.nome});

  Unidade.empty({this.id = 1});

  Unidade.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    return data;
  }
}
