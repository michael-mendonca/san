import 'package:SAN/models/usuario.dart';

class Paciente {
  int id;
  int nutricionistaId;
  String createdAt;
  String updatedAt;
  Usuario usuario;

  Paciente(
      {this.id,
      this.nutricionistaId,
      this.createdAt,
      this.updatedAt,
      this.usuario});

  Paciente.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nutricionistaId = json['nutricionista_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    usuario =
        json['usuario'] != null ? new Usuario.fromJson(json['usuario']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nutricionista_id'] = this.nutricionistaId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.usuario != null) {
      data['usuario'] = this.usuario.toJson();
    }
    return data;
  }
}
