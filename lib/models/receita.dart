import 'package:SAN/models/alimento.dart';

class Receita {
  int id;
  int usuarioId;
  String nome;
  String descricao;
  String preparo;
  bool publico;
  List<Alimento> alimentos;
  num porcoes;
  String createdAt;
  String updatedAt;
  InfoNutricionais infoNutricionais;
  PivotRefeicao pivotRefeicao;

  Receita(
      {this.id,
      this.usuarioId,
      this.nome,
      this.descricao,
      this.preparo,
      this.publico,
      this.createdAt,
      this.updatedAt,
      this.alimentos,
      this.pivotRefeicao});

  Receita.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    usuarioId = json['usuario_id'];
    nome = json['nome'];
    descricao = json['descricao'];
    preparo = json['preparo'];
    publico = json['publico'] == 0 ? false : true;
    porcoes = json['porcoes'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['alimentos'] != null) {
      alimentos = new List<Alimento>();
      json['alimentos'].forEach((v) {
        alimentos.add(new Alimento.fromJson(v));
      });
    }
    pivotRefeicao = json['pivot'] != null
        ? new PivotRefeicao.fromJson(json['pivot'])
        : null;
    infoNutricionais = calcularNutrientes();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['usuario_id'] = this.usuarioId;
    data['nome'] = this.nome;
    data['descricao'] = this.descricao;
    data['preparo'] = this.preparo;
    data['porcoes'] = this.porcoes;
    data['publico'] = this.publico ? 1 : 0;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.alimentos != null) {
      data['alimentos'] = this.alimentos.map((v) => v.toJson()).toList();
    }
    if (this.pivotRefeicao != null) {
      data['pivot'] = this.pivotRefeicao.toJson();
    }
    return data;
  }

  InfoNutricionais calcularNutrientes() {
    InfoNutricionais nutrientes = InfoNutricionais();

    this.alimentos.forEach((alimento) => {
          nutrientes.kcal += (alimento.kcal * alimento.pivotReceita.porcoes),
          nutrientes.proteina +=
              (alimento.proteina * alimento.pivotReceita.porcoes),
          nutrientes.lipideos +=
              (alimento.lipideos * alimento.pivotReceita.porcoes),
          nutrientes.colesterol +=
              (alimento.colesterol * alimento.pivotReceita.porcoes),
          nutrientes.carboidratos +=
              (alimento.carboidratos * alimento.pivotReceita.porcoes),
          nutrientes.fibra += (alimento.fibra * alimento.pivotReceita.porcoes),
          nutrientes.cinzas +=
              (alimento.cinzas * alimento.pivotReceita.porcoes),
          nutrientes.calcio +=
              (alimento.calcio * alimento.pivotReceita.porcoes),
          nutrientes.magnesio +=
              (alimento.magnesio * alimento.pivotReceita.porcoes),
          nutrientes.manganes +=
              (alimento.manganes * alimento.pivotReceita.porcoes),
          nutrientes.fosforo +=
              (alimento.fosforo * alimento.pivotReceita.porcoes),
          nutrientes.ferro += (alimento.ferro * alimento.pivotReceita.porcoes),
          nutrientes.sodio += (alimento.sodio * alimento.pivotReceita.porcoes),
          nutrientes.potassio +=
              (alimento.potassio * alimento.pivotReceita.porcoes),
          nutrientes.cobre += (alimento.cobre * alimento.pivotReceita.porcoes),
          nutrientes.zinco += (alimento.zinco * alimento.pivotReceita.porcoes),
        });

    return nutrientes;
  }
}

class InfoNutricionais {
  double kcal;
  double proteina;
  double lipideos;
  double colesterol;
  double carboidratos;
  double fibra;
  double cinzas;
  double calcio;
  double magnesio;
  double manganes;
  double fosforo;
  double ferro;
  double sodio;
  double potassio;
  double cobre;
  double zinco;

  InfoNutricionais(
      {this.kcal = 0,
      this.proteina = 0,
      this.lipideos = 0,
      this.colesterol = 0,
      this.carboidratos = 0,
      this.fibra = 0,
      this.cinzas = 0,
      this.calcio = 0,
      this.magnesio = 0,
      this.manganes = 0,
      this.fosforo = 0,
      this.ferro = 0,
      this.sodio = 0,
      this.potassio = 0,
      this.cobre = 0,
      this.zinco = 0});
}

class PivotRefeicao {
  int receitaId;
  int refeicaoId;
  int porcoes;

  PivotRefeicao({this.receitaId, this.refeicaoId, this.porcoes});

  PivotRefeicao.fromJson(Map<String, dynamic> json) {
    receitaId = json['receita_id'];
    refeicaoId = json['refeicao_id'];
    porcoes = json['porcoes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['receita_id'] = this.receitaId;
    data['refeicao_id'] = this.refeicaoId;
    data['porcoes'] = this.porcoes;
    return data;
  }
}
