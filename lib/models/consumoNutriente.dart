import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class ConsumoChart {
  String nutriente;
  double consumo;
  charts.Color cor;

  ConsumoChart(this.nutriente, this.consumo, this.cor);

  ConsumoChart.fromJson(Map<String, dynamic> json) {
    nutriente = json['nutriente'];
    consumo = json['consumo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nutriente'] = this.nutriente;
    data['consumo'] = this.consumo;
    return data;
  }
}

class Consumo {
  int kcal;
  double proteina;
  double lipideos;
  int colesterol;
  double carboidratos;
  double fibra;
  double cinzas;
  int calcio;
  int magnesio;
  double manganes;
  int fosforo;
  double ferro;
  int sodio;
  int potassio;
  double cobre;
  double zinco;

  Consumo(
      {this.kcal,
      this.proteina,
      this.lipideos,
      this.colesterol,
      this.carboidratos,
      this.fibra,
      this.cinzas,
      this.calcio,
      this.magnesio,
      this.manganes,
      this.fosforo,
      this.ferro,
      this.sodio,
      this.potassio,
      this.cobre,
      this.zinco});

  Consumo.fromJson(Map<String, dynamic> json) {
    kcal = json['kcal'];
    proteina = json['proteina'];
    lipideos = json['lipideos'];
    colesterol = json['colesterol'];
    carboidratos = json['carboidratos'];
    fibra = json['fibra'];
    cinzas = json['cinzas'];
    calcio = json['calcio'];
    magnesio = json['magnesio'];
    manganes = json['manganes'];
    fosforo = json['fosforo'];
    ferro = json['ferro'];
    sodio = json['sodio'];
    potassio = json['potassio'];
    cobre = json['cobre'];
    zinco = json['zinco'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kcal'] = this.kcal;
    data['proteina'] = this.proteina;
    data['lipideos'] = this.lipideos;
    data['colesterol'] = this.colesterol;
    data['carboidratos'] = this.carboidratos;
    data['fibra'] = this.fibra;
    data['cinzas'] = this.cinzas;
    data['calcio'] = this.calcio;
    data['magnesio'] = this.magnesio;
    data['manganes'] = this.manganes;
    data['fosforo'] = this.fosforo;
    data['ferro'] = this.ferro;
    data['sodio'] = this.sodio;
    data['potassio'] = this.potassio;
    data['cobre'] = this.cobre;
    data['zinco'] = this.zinco;
    return data;
  }
}
