import 'package:SAN/models/paciente.dart';
import 'package:SAN/models/nutricionista.dart';
import 'package:SAN/models/planejamento.dart';
import 'package:SAN/models/refeicao.dart';
import 'package:flutter/material.dart';

class Dieta {
  int id;
  String descricao;
  String createdAt;
  String updatedAt;
  Nutricionista nutricionista;
  Paciente paciente;
  List<Planejamento> planejamentos;

  Dieta.empty()
      : this(planejamentos: [
          Planejamento(
              refeicao: Refeicao.cafe(),
              horario: TimeOfDay(hour: 09, minute: 00)),
          Planejamento(
              refeicao: Refeicao.almoco(),
              horario: TimeOfDay(hour: 12, minute: 00)),
          Planejamento(
              refeicao: Refeicao.lanche(),
              horario: TimeOfDay(hour: 15, minute: 00)),
          Planejamento(
              refeicao: Refeicao.jantar(),
              horario: TimeOfDay(hour: 18, minute: 00)),
        ]);

  Dieta(
      {this.id,
      this.descricao,
      this.createdAt,
      this.updatedAt,
      this.nutricionista,
      this.paciente,
      this.planejamentos});

  Dieta.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    descricao = json['descricao'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    nutricionista = json['nutricionista'] != null
        ? new Nutricionista.fromJson(json['nutricionista'])
        : null;
    paciente = json['paciente'] != null
        ? new Paciente.fromJson(json['paciente'])
        : null;
    if (json['planejamentos'] != null) {
      planejamentos = new List<Planejamento>();
      json['planejamentos'].forEach((v) {
        planejamentos.add(new Planejamento.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['descricao'] = this.descricao;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.nutricionista != null) {
      data['nutricionista'] = this.nutricionista.toJson();
    }
    if (this.paciente != null) {
      data['paciente'] = this.paciente.toJson();
    }
    if (this.planejamentos != null) {
      data['planejamentos'] =
          this.planejamentos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
