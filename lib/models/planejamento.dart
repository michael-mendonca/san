import 'package:SAN/models/refeicao.dart';
import 'package:flutter/material.dart';

class Planejamento {
  int id;
  int dietaId;
  TimeOfDay horario;
  String createdAt;
  String updatedAt;
  Refeicao refeicao;

  Planejamento(
      {this.id,
      this.dietaId,
      this.horario,
      this.createdAt,
      this.updatedAt,
      this.refeicao});

  Planejamento.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    dietaId = json['dieta_id'];
    var s = json['horario'];
    horario = TimeOfDay(
        hour: int.parse(s.split(":")[0]), minute: int.parse(s.split(":")[1]));
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    refeicao = json['refeicao'] != null
        ? new Refeicao.fromJson(json['refeicao'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['dieta_id'] = this.dietaId;
    // data['horario'] = this.horario;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.refeicao != null) {
      data['refeicao'] = this.refeicao.toJson();
    }
    if (this.horario != null) {
      data['horario'] = '${this.horario.hour}:${this.horario.minute}:00';
    }
    return data;
  }
}
