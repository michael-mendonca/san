class Usuario {
  int id;
  String nome;
  String email;
  String senha;
  int perfil;
  String createdAt;
  String updatedAt;

  Usuario(
      {this.id,
      this.nome,
      this.email,
      this.senha,
      this.perfil,
      this.createdAt,
      this.updatedAt});

  Usuario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nome = json['nome'];
    email = json['email'];
    senha = json['senha'];
    perfil = json['perfil'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    data['email'] = this.email;
    data['senha'] = this.senha;
    data['perfil'] = this.perfil;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
