import 'package:SAN/models/usuario.dart';

class Nutricionista {
  int id;
  Usuario usuario;
  String crn;
  String createdAt;
  String updatedAt;

  Nutricionista(
      {this.id, this.usuario, this.crn, this.createdAt, this.updatedAt});

  Nutricionista.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    crn = json['crn'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    usuario =
        json['usuario'] != null ? new Usuario.fromJson(json['usuario']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['crn'] = this.crn;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.usuario != null) {
      data['usuario'] = this.usuario.toJson();
    }
    return data;
  }
}
