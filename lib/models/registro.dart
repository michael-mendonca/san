import 'package:SAN/models/refeicao.dart';

class Registro {
  int id;
  int pacienteId;
  DateTime data;
  String createdAt;
  String updatedAt;
  Refeicao refeicao;

  Registro(
      {this.id,
      this.pacienteId,
      this.data,
      this.createdAt,
      this.updatedAt,
      this.refeicao});

  Registro.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pacienteId = json['paciente_id'];
    data = json['data'] != null ? DateTime.tryParse(json['data']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    refeicao = json['refeicao'] != null
        ? new Refeicao.fromJson(json['refeicao'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['paciente_id'] = this.pacienteId;
    // data['data'] = this.data;
    if (this.data != null) {
      data['data'] =
          "${this.data.year}-${this.data.month}-${this.data.day} ${this.data.hour}:${this.data.minute}:00";
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.refeicao != null) {
      data['refeicao'] = this.refeicao.toJson();
    }
    return data;
  }
}
