import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:SAN/components/alimentosList.dart';
import 'package:SAN/models/antropometria.dart';
import 'package:SAN/models/consumoNutriente.dart';
import 'package:SAN/models/nutricionista.dart';
import 'package:SAN/models/paciente.dart';
import 'package:SAN/models/receita.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:convert';

import 'models/alimento.dart';
import 'models/dieta.dart';
import 'models/registro.dart';
import 'models/usuario.dart';

const baseUrl = "http://10.0.2.2:3333";

class API {
  Future<Usuario> login(String email, senha) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var response = await http
        .post(baseUrl + "/login", body: {'email': email, 'senha': senha});

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);

      var token = jsonData['token'];

      sharedPreferences.setString("token", token);

      var url = baseUrl + "/usuarioOnline";
      var response2 = await http.get(url,
          headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

      Map<String, dynamic> usuarioData = jsonDecode(response2.body);
      var usuario = Usuario.fromJson(usuarioData);

      print('Login realizado com sucesso ( ' + usuario.nome + ' )');

      return usuario;
    } else {
      print(response.body);
      return null;
    }
  }

  Future<List<Paciente>> getPacientes() async {
    var url = baseUrl + "/pacientes";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

    Map<String, dynamic> map = json.decode(response.body);

    List<dynamic> data = map["pacientes"];

    List<Paciente> pacientes = [];

    for (var p in data) {
      Paciente paciente = Paciente.fromJson(p);

      // print(paciente.id);

      pacientes.add(paciente);
    }

    //print("Tamanho: " + pacientes.length.toString());

    return pacientes;
  }

  Future<List<Dieta>> getDietasPaciente(int pacienteId) async {
    var url = baseUrl + "/dietasAtivasPaciente/" + pacienteId.toString();

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

    Map<String, dynamic> map = json.decode(response.body);

    List<dynamic> data = map["dietas"];

    List<Dieta> dietas = [];

    for (var d in data) {
      Dieta dieta = Dieta.fromJson(d);

      // print(dieta.id);

      dietas.add(dieta);
    }

    //print("Tamanho: " + pacientes.length.toString());

    return dietas;
  }

  Future<List<Dieta>> getDietasPacienteOnline() async {
    var url = baseUrl + "/dietasPacienteOnline";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

    Map<String, dynamic> map = json.decode(response.body);

    List<dynamic> data = map["dietas"];

    List<Dieta> dietas = [];

    for (var d in data) {
      Dieta dieta = Dieta.fromJson(d);

      // print(dieta.id);

      dietas.add(dieta);
    }

    //print("Tamanho: " + pacientes.length.toString());

    return dietas;
  }

  Future<Dieta> getDieta(int dietaId) async {
    var url = baseUrl + "/dieta/" + dietaId.toString();

    // print("Dieta id: $dietaId");

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

    // print("Response (" + response.statusCode.toString() + ")");
    // print("Body (" + response.body + ")");

    try {
      if (response.statusCode == 200) {
        // print("Registro salvo: ");

        Map<String, dynamic> map = jsonDecode(response.body);
        List<dynamic> data = map["dieta"];

        Dieta dieta = Dieta.fromJson(data[0]);

        // print(dieta.toJson().toString());

        return dieta;
      }
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  addAlimentoRefeicao(int idRefeicao, String idAlimento) async {
    var url =
        baseUrl + "/addAlimento/" + idRefeicao.toString() + "/" + idAlimento;

    var response = await http.post(url);

    print(response);
  }

  Future<List<Alimento>> listAlimentos() async {
    var url = baseUrl + "/listAlimentos";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    var token = sharedPreferences.getString("token");

    var response = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

    Map<String, dynamic> map = json.decode(response.body);

    List<dynamic> data = map["alimentos"];

    List<Alimento> alimentos = [];

    for (var d in data) {
      Alimento alimento = Alimento.fromJson(d);

      //print("Alimento: : " + alimento.nome);

      alimentos.add(alimento);
    }

    return alimentos;
  }

  Future<List<dynamic>> buscaAlimentos(String busca) async {
    var url = baseUrl + "/buscaAlimentos";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    var token = sharedPreferences.getString("token");

    var response = await http.post(
      url,
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + token,
        HttpHeaders.contentTypeHeader: "application/json",
      },
      body: jsonEncode(<String, String>{
        'busca': busca,
      }),
    );

    Map<String, dynamic> map = json.decode(response.body);

    List<dynamic> resultado = [];

    try {
      List<dynamic> dataAlimentos = map["alimentos"];

      List<Alimento> alimentos = [];

      for (var d in dataAlimentos) {
        Alimento alimento = Alimento.fromJson(d);

        alimentos.add(alimento);
      }

      List<dynamic> dataReceitas = map["receitas"];

      List<Receita> receitas = [];

      for (var d in dataReceitas) {
        Receita receita = Receita.fromJson(d);

        // print("Receita: " + d.nome);
        receitas.add(receita);
      }

      print(alimentos);
      print(receitas);

      resultado.addAll(alimentos);
      resultado.addAll(receitas);

      print("Resultado: ");

      for (var r in resultado) {
        print(r.runtimeType.toString());
        if (r.runtimeType == Alimento) {
          print(r.nome + " - " + r.detalhe);
        } else if (r.runtimeType == Receita) {
          print(r.nome + " - " + r.descricao);
        }
      }
    } catch (e) {
      print(e);
    }

    return resultado;
  }

  Future<Dieta> createDieta(Dieta dieta) async {
    var url = baseUrl + "/createDieta";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + token,
          HttpHeaders.contentTypeHeader: "application/json",
        },
        body: jsonEncode(<String, dynamic>{
          'dieta': dieta,
        }));

    if (response.statusCode == 200) {
      final String responseString = response.body;

      Map<String, dynamic> map = json.decode(responseString);

      List<dynamic> data = map["dieta"];

      var dietaReturn;
      for (var d in data) {
        dietaReturn = Dieta.fromJson(d);

        // print("Id dieta: : " + dieta.id.toString());

        // dietas.add(dieta);
      }

      return dietaReturn;
    } else {
      return null;
    }
  }

  Future<Registro> saveOrUpdateRegistro(Registro registro) async {
    var url;

    // print("Registro = " + registro.toJson().toString());

    if (registro.id == null) {
      print("Criando registro...");
      url = baseUrl + "/createRegistro";
    } else {
      print("Atualizando registro " + registro.id.toString() + "...");
      url = baseUrl + "/updateRegistro";
    }

    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");

      var response = await http.post(url,
          headers: {
            HttpHeaders.authorizationHeader: "Bearer " + token,
            HttpHeaders.contentTypeHeader: "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            'registro': registro,
          }));

      print("Response (" + response.statusCode.toString() + ")");

      if (response.statusCode == 200) {
        print("Registro salvo: ");
        Map<String, dynamic> registroData = jsonDecode(response.body);
        var registroSalvo = Registro.fromJson(registroData);

        print(registroSalvo.toJson().toString());

        return registroSalvo;
      }

      return null;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  Future<List<Registro>> registrosAtuais() async {
    var url = baseUrl + '/registrosAtuais';

    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");

      var response = await http.get(url,
          headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

      List<dynamic> data = json.decode(response.body);

      List<Registro> registros = [];
      for (var d in data) {
        Registro registro = Registro.fromJson(d);

        registros.add(registro);
      }
      // List<Registro> registros = data.map((s) => s as Registro).toList();
      // for (var d in data) {
      //   print(d.toString());
      // }

      return registros;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  removerAlimento(int refeicao_id, int alimento_id) async {
    var url = baseUrl + '/removeAlimento/$refeicao_id/$alimento_id';

    var response = await http.post(url);

    print(response);
  }

  removerReceita(int refeicao_id, int receita_id) async {
    var url = baseUrl + '/removeReceita/$refeicao_id/$receita_id';

    var response = await http.post(url);

    print(response);
  }

  Future<Nutricionista> nutricionistaLogado() async {
    var url = baseUrl + '/nutricionistaOnline';

    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");

      var response = await http.get(url,
          headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

      // print("Response (" + response.statusCode.toString() + ")");

      if (response.statusCode == 200) {
        // print("Registro salvo: ");
        Map<String, dynamic> nutricionistaData = jsonDecode(response.body);
        var nutricionistaSalvo = Nutricionista.fromJson(nutricionistaData);

        print(nutricionistaSalvo.toJson().toString());

        return nutricionistaSalvo;
      }

      return null;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  Future<List<ConsumoChart>> consumoMedio(int paciente_id) async {
    var url = baseUrl + '/consumoMedio';

    print("Consumo médio paciente: $paciente_id");

    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");

      var response = await http.post(url,
          headers: {
            HttpHeaders.authorizationHeader: "Bearer " + token,
            HttpHeaders.contentTypeHeader: "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            "paciente_id": paciente_id,
          }));

      print("Response (" + response.statusCode.toString() + ")");
      print("Body (" + response.body + ")");

      List<dynamic> data = json.decode(response.body);

      List<ConsumoChart> consumos = [];
      for (var d in data) {
        ConsumoChart consumo = ConsumoChart.fromJson(d);

        consumos.add(consumo);
      }

      return consumos;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  Future<List<Dieta>> getDietaAtivaPacienteOnline() async {
    var url = baseUrl + "/dietaAtivaPacienteOnline";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

    Map<String, dynamic> map = json.decode(response.body);

    List<dynamic> data = map["dieta"];

    List<Dieta> dietas = [];

    for (var d in data) {
      Dieta dieta = Dieta.fromJson(d);

      // print(dieta.id);

      dietas.add(dieta);
    }

    //print("Tamanho: " + pacientes.length.toString());

    return dietas;
  }

  Future<List<Antropometria>> getAntropometriasPaciente(int pacienteId) async {
    print("getAntropometriasPaciente");

    var url = baseUrl + "/antropometriasPaciente/" + pacienteId.toString();
    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");

      var response = await http.get(url,
          headers: {HttpHeaders.authorizationHeader: "Bearer " + token});

      Map<String, dynamic> map = json.decode(response.body);

      List<dynamic> data = map["antropometrias"];

      List<Antropometria> antropometrias = [];

      for (var d in data) {
        Antropometria antro = Antropometria.fromJson(d);

        antropometrias.add(antro);
      }

      // print("Resultados: ");
      // print(antropometrias);

      return antropometrias;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  Future<Antropometria> createAntropometria(Antropometria antro) async {
    var url = baseUrl + "/createAntropometria";

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + token,
          HttpHeaders.contentTypeHeader: "application/json",
        },
        body: jsonEncode(<String, dynamic>{
          'antropometria': antro,
        }));

    if (response.statusCode == 200) {
      final String responseString = response.body;

      Map<String, dynamic> map = json.decode(responseString);

      List<dynamic> data = map["antropometria"];

      var antroReturn;
      for (var d in data) {
        antroReturn = Antropometria.fromJson(d);
      }

      return antroReturn;
    } else {
      return null;
    }
  }

  Future<Alimento> createAlimento(Alimento alimento) async {
    var url = baseUrl + "/createAlimento";

    print("Criando alimento");
    print("Alimento: " + alimento.toJson().toString());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");

    var response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + token,
          HttpHeaders.contentTypeHeader: "application/json",
        },
        body: jsonEncode(<String, dynamic>{
          'alimento': alimento,
        }));

    if (response.statusCode == 200) {
      print("Alimento cadastrado:");
      Map<String, dynamic> alimentoData = jsonDecode(response.body);
      var aliReturn = Alimento.fromJson(alimentoData);

      print(aliReturn);

      return aliReturn;
    } else {
      print("Falha ao cadastrar alimento");
      return null;
    }
  }

  Future<List<Unidade>> listUnidades() async {
    var url = baseUrl + "/listUnidades";
    try {
      var response = await http.get(url);

      Map<String, dynamic> map = json.decode(response.body);

      List<dynamic> data = map["unidades"];

      List<Unidade> unidades = [];

      for (var d in data) {
        Unidade unidade = Unidade.fromJson(d);

        unidades.add(unidade);
      }

      // print("Resultados: ");
      // print(antropometrias);

      return unidades;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }

  Future<List<ConsumoChart>> consumoPeriodo(
      int pacienteId, String dataIni, String dataFim) async {
    var url = baseUrl + '/consumoPeriodo';

    print("Consumo período paciente: $pacienteId");

    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      var token = sharedPreferences.getString("token");

      var response = await http.post(url,
          headers: {
            HttpHeaders.authorizationHeader: "Bearer " + token,
            HttpHeaders.contentTypeHeader: "application/json",
          },
          body: jsonEncode(<String, dynamic>{
            "paciente_id": pacienteId,
            "dataIni": dataIni,
            "dataFim": dataFim
          }));

      print("Response (" + response.statusCode.toString() + ")");
      log("Body (" + response.body + ")");

      var data = json.decode(json.encode(response.body));

      print("Data" + data.toString());

      var consumoData = Consumo.fromJson(data);

      print(data);

      List<ConsumoChart> result = [];

      List<charts.Color> cores = [
        charts.Color.fromHex(code: '#6929c4'),
        charts.Color.fromHex(code: '#1192e8'),
        charts.Color.fromHex(code: '#005d5d'),
        charts.Color.fromHex(code: '#9f1853'),
        charts.Color.fromHex(code: '#fa4d56'),
        charts.Color.fromHex(code: '#570408'),
        charts.Color.fromHex(code: '#198038'),
        charts.Color.fromHex(code: '#002d9c'),
        charts.Color.fromHex(code: '#ee538b'),
        charts.Color.fromHex(code: '#b28600'),
        charts.Color.fromHex(code: '#009d9a'),
        charts.Color.fromHex(code: '#012749'),
        charts.Color.fromHex(code: '#8a3800'),
        charts.Color.fromHex(code: '#a56eff'),
      ];

      var corSelecionada = 0;

      data.forEach((k, v) {
        result.add(new ConsumoChart(k, v, cores[corSelecionada]));
        corSelecionada++;
      });

      return result;
    } catch (e, stacktrace) {
      print('Exception: ' + e.toString());
      print('Stacktrace: ' + stacktrace.toString());
      return null;
    }
  }
}
