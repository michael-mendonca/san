import 'dart:ui';
import 'package:flutter/material.dart';

class Global {
  static const mainColor = MaterialColor(0x00BF6F, <int, Color>{
    50: Color(0xDDF6EC),
    100: Color(0xD5F4E7),
    200: Color(0xCAF1E1),
    300: Color(0xBDEED9),
    400: Color(0xACEAD0),
    500: Color(0x97E5C4),
    600: Color(0x7DDEB5),
    700: Color(0x5CD6A3),
    800: Color(0x33CC8C),
    900: Color(0x00BF6F),
  });
}
