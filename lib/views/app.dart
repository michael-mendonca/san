import 'package:SAN/views/alimento/adicionar_alimento_page.dart';
import 'package:SAN/views/alimento/adicionar_receita_page.dart';
import 'package:SAN/views/alimento/alimento_busca_page.dart';
import 'package:SAN/views/alimento/cad_alimento_page.dart';
import 'package:SAN/views/app_controller.dart';
import 'package:SAN/views/globals.dart';
import 'package:SAN/views/nutricionista/antro_cad_page.dart';
import 'package:SAN/views/nutricionista/diet_cad_page.dart';
import 'package:SAN/views/nutricionista/nutri_home_page.dart';
import 'package:SAN/views/login_page.dart';
import 'package:SAN/views/nutricionista/nutri_pacientes_page.dart';
import 'package:SAN/views/nutricionista/paci_antro_page.dart';
import 'package:SAN/views/paciente/paci_diet_info_page.dart';
import 'package:flutter/material.dart';
import 'package:SAN/views/nutricionista/paci_menu_page.dart';
import 'package:SAN/views/nutricionista/diet_info_page.dart';
import 'nutricionista/paci_consumo_page.dart';
import 'nutricionista/paci_diet_page.dart';

import 'paciente/paci_diet_page.dart';
import 'paciente/paci_home_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: AppController.instance,
      builder: (context, child) {
        return MaterialApp(
          title: 'SAN App',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            // primaryColor: Colors.white,
            // primaryColorDark: Colors.white,
            canvasColor: Colors.grey.shade200,
            primarySwatch: Colors.green,
            brightness: AppController.instance.isDarkTheme
                ? Brightness.dark
                : Brightness.light,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: '/',
          routes: {
            '/': (context) => LoginPage(),
            // Nutricionista
            '/nutriHome': (context) => NutriHomePage(),
            '/nutriPacientes': (context) => NutriPacientesPage(),
            '/pacienteMenu': (context) => PacienteMenuPage(),
            '/nutriPacienteDietas': (context) => NutriPacienteDietasPage(),
            '/pacienteConsumo': (context) => PaciConsumoPage(),
            '/dietaInfo': (context) => DietaInfoPage(),
            '/dietaCadastro': (context) => DietaCadastroPage(),
            '/nutriPacienteAntropometrias': (context) =>
                NutriPacienteAntropometriasPage(),
            '/antropometriaCadastro': (context) => AntropometriaCadastroPage(),
            // Paciente
            '/paciHome': (context) => PaciHomePage(),
            '/pacienteDietas': (context) => PacienteDietasPage(),
            '/dietaPaciInfo': (context) => DietaInfoPaciPage(),
            // Alimentos
            '/alimentoBusca': (context) => AlimentoBuscaPage(),
            '/adicionarAlimento': (context) => AdicionarAlimentoPage(),
            '/adicionarReceita': (context) => AdicionarReceitaPage(),
            '/createAlimento': (context) => AlimentoCadastroPage(),
          },
        );
      },
    );
  }
}
