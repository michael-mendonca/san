import 'package:SAN/components/paciNavigationBar.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import 'package:intl/intl.dart';

class PacienteDietasPage extends StatefulWidget {
  @override
  _PacienteDietasPageState createState() => _PacienteDietasPageState();

  const PacienteDietasPage({Key key}) : super(key: key);
}

class _PacienteDietasPageState extends State<PacienteDietasPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SanAppBar(titulo: 'Dietas do Paciente'),
      body: Container(
          child: Column(
        children: [
          Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Dietas:',
                    style: TextStyle(
                        fontSize: 20,
                        color: Color.fromRGBO(125, 125, 125, 1.0)),
                  ),
                ],
              )),
          Expanded(
            child: FutureBuilder(
                future: API().getDietasPacienteOnline(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                        child: Center(
                      child: Text("Carregando..."),
                    ));
                  } else {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                            tileColor: Colors.white,
                            leading: Icon(
                              Icons.list,
                              size: 50,
                            ),
                            title: Text(snapshot.data[index].descricao),
                            subtitle: Text(DateFormat('dd/MM/yyyy').format(
                                DateTime.parse(
                                    snapshot.data[index].createdAt))),
                            trailing: Icon(Icons.keyboard_arrow_right),
                            onTap: () {
                              Navigator.of(context).pushNamed('/dietaPaciInfo',
                                  arguments: snapshot.data[index].id);
                            });
                      },
                    );
                  }
                }),
          ),
        ],
      )),
      bottomNavigationBar: PaciNavigationBar(2),
    );
  }
}
