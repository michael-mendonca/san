import 'package:SAN/components/paciNavigationBar.dart';
import 'package:SAN/models/alimento.dart';
import 'package:SAN/models/receita.dart';
import 'package:SAN/models/refeicao.dart';
import 'package:SAN/models/registro.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import "package:intl/intl.dart";

import '../../api.dart';

class PaciHomePage extends StatefulWidget {
  @override
  _PaciHomePageState createState() => _PaciHomePageState();

  const PaciHomePage({Key key}) : super(key: key);
}

class _PaciHomePageState extends State<PaciHomePage> {
  // List<Registro> registros = registroDiario();
  var caloriasTotais;

  @override
  Widget build(BuildContext context) {
    var f = new NumberFormat("###.##", "pt_BR");

    return Scaffold(
      appBar: SanAppBar(
        titulo: 'Registro Diário',
      ),
      body: Container(
          child: Column(
        children: [
          Expanded(
            child: FutureBuilder(
                future: registroDiario(),
                builder: (BuildContext context, AsyncSnapshot registros) {
                  if (registros.data == null) {
                    return Container(
                        child: Center(
                      child: Text("Carregando..."),
                    ));
                  } else {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Container(
                            width: 150,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                border:
                                    Border.all(color: Colors.grey.shade300)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Text(
                                    "Calorias consumidas: ",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey.shade400),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        f.format(
                                            caloriasConsumidas(registros.data)),
                                        style: TextStyle(
                                            fontSize: 25,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(" kcal"),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                                itemCount: registros.data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Card(
                                    child: Column(
                                      children: [
                                        Container(
                                          color: Colors.green,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                    '${registros.data[index].refeicao.categoria.nome}',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 18)),
                                                RaisedButton(
                                                    onPressed: () async {
                                                      adicionarAlimento(
                                                          registros.data[index],
                                                          context,
                                                          index);

                                                      setState(() {});
                                                    },
                                                    color: Colors.transparent,
                                                    elevation: 0,
                                                    child: Icon(Icons.add,
                                                        color: Colors.white))
                                              ],
                                            ),
                                          ),
                                        ),
                                        ListView.builder(
                                          itemCount: registros.data[index]
                                              .refeicao.alimentos.length,
                                          itemBuilder: (context, index2) {
                                            return Dismissible(
                                              key: UniqueKey(),
                                              child: ListTile(
                                                title: Text(registros
                                                    .data[index]
                                                    .refeicao
                                                    .alimentos[index2]
                                                    .nome),
                                                subtitle: Text(
                                                    '${registros.data[index].refeicao.alimentos[index2].detalhe}, ${registros.data[index].refeicao.alimentos[index2].quantidade * registros.data[index].refeicao.alimentos[index2].pivotRefeicao.porcoes}${registros.data[index].refeicao.alimentos[index2].unidade.nome}'),
                                                trailing: Text(
                                                    '${f.format(registros.data[index].refeicao.alimentos[index2].kcal * registros.data[index].refeicao.alimentos[index2].pivotRefeicao.porcoes)}kcal'),
                                              ),
                                              onDismissed: (direction) async {
                                                await API().removerAlimento(
                                                    registros.data[index]
                                                        .refeicao.id,
                                                    registros
                                                        .data[index]
                                                        .refeicao
                                                        .alimentos[index2]
                                                        .id);
                                                setState(() {
                                                  registros.data[index].refeicao
                                                      .alimentos
                                                      .removeAt(index2);
                                                });

                                                Scaffold.of(context)
                                                    .showSnackBar(SnackBar(
                                                        content: Text(
                                                            "Alimento removido")));
                                              },
                                              background:
                                                  Container(color: Colors.red),
                                            );
                                          },
                                          shrinkWrap: true,
                                          physics: ClampingScrollPhysics(),
                                        ),
                                        ListView.builder(
                                          itemCount: registros.data[index]
                                              .refeicao.receitas.length,
                                          itemBuilder: (context, index2) {
                                            return Dismissible(
                                              key: UniqueKey(),
                                              child: ListTile(
                                                title: Text(registros
                                                    .data[index]
                                                    .refeicao
                                                    .receitas[index2]
                                                    .nome),
                                                subtitle: Text(
                                                    '${registros.data[index].refeicao.receitas[index2].descricao}, ${registros.data[index].refeicao.receitas[index2].pivotRefeicao.porcoes} ${registros.data[index].refeicao.receitas[index2].pivotRefeicao.porcoes > 1 ? "porções" : "porção"}'),
                                                trailing: Text(
                                                    '${f.format(registros.data[index].refeicao.receitas[index2].infoNutricionais.kcal * registros.data[index].refeicao.receitas[index2].pivotRefeicao.porcoes)}kcal'),
                                              ),
                                              onDismissed: (direction) async {
                                                await API().removerReceita(
                                                    registros.data[index]
                                                        .refeicao.id,
                                                    registros
                                                        .data[index]
                                                        .refeicao
                                                        .receitas[index2]
                                                        .id);
                                                setState(() {
                                                  registros.data[index].refeicao
                                                      .receitas
                                                      .removeAt(index2);
                                                });

                                                Scaffold.of(context)
                                                    .showSnackBar(SnackBar(
                                                        content: Text(
                                                            "Receita removida")));
                                              },
                                              background:
                                                  Container(color: Colors.red),
                                            );
                                          },
                                          shrinkWrap: true,
                                          physics: ClampingScrollPhysics(),
                                        )
                                      ],
                                    ),
                                  );
                                }),
                          ),
                        ],
                      ),
                    );
                  }
                }),
          )
        ],
      )),
      bottomNavigationBar: PaciNavigationBar(0),
    );
  }

  adicionarAlimento(Registro registro, BuildContext context, int index) async {
    Navigator.of(context).pushNamed('/alimentoBusca').then((result) async {
      if (result != null) {
        if (result is Alimento) {
          print("Alimento ${result.nome} retornado");
          registro.refeicao.alimentos.add(result);
        } else if (result is Receita) {
          print("Receita ${result.nome} retornada");
          registro.refeicao.receitas.add(result);
        }

        Registro registroSalvo = await API().saveOrUpdateRegistro(registro);
        if (registroSalvo != null) {
          setState(() {});
          registro = registroSalvo;
        } else {
          registro.refeicao.alimentos.remove(result);
        }
      } else {
        print("Alimento não reconhecido - paciHome");
      }
    });
  }

  num caloriasConsumidas(List<Registro> registros) {
    num consumo = 0.00;
    if (registros != null && registros.isNotEmpty) {
      // print("Entrou em registros");
      registros.forEach((reg) {
        if (reg.refeicao.alimentos != null &&
            reg.refeicao.alimentos.isNotEmpty) {
          reg.refeicao.alimentos.forEach((ali) {
            // print("Alimento: " + ali.nome);
            consumo += ali.kcal * ali.pivotRefeicao.porcoes;
          });
        }

        if (reg.refeicao.receitas != null && reg.refeicao.receitas.isNotEmpty) {
          reg.refeicao.receitas.forEach((rec) {
            // print("Receita: " + rec.nome);
            consumo += rec.infoNutricionais.kcal * rec.pivotRefeicao.porcoes;
          });
        }
      });
    }
    // print(consumo.toString());

    return consumo;
  }
}

Future<List<Registro>> registroDiario() async {
  List<Registro> registrosList = [];
  DateTime now = new DateTime.now();

  registrosList = await API().registrosAtuais();

  var listCategorias = [];
  registrosList.forEach((Registro reg) {
    listCategorias.add(reg.refeicao.categoria.id);
  });

  if (!listCategorias.contains(1)) {
    registrosList.add(Registro(
        data: new DateTime(now.year, now.month, now.day, 0, 0, 0),
        refeicao: Refeicao.cafe()));
  }
  if (!listCategorias.contains(2)) {
    registrosList.add(Registro(
        data: new DateTime(now.year, now.month, now.day, 0, 0, 0),
        refeicao: Refeicao.almoco()));
  }
  if (!listCategorias.contains(3)) {
    registrosList.add(Registro(
        data: new DateTime(now.year, now.month, now.day, 0, 0, 0),
        refeicao: Refeicao.jantar()));
  }
  if (!listCategorias.contains(4)) {
    registrosList.add(Registro(
        data: new DateTime(now.year, now.month, now.day, 0, 0, 0),
        refeicao: Refeicao.lanche()));
  }

  return registrosList;
}
