import 'package:SAN/components/paciNavigationBar.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';

class PacienteInfoPage extends StatefulWidget {
  @override
  _PacienteInfoPageState createState() => _PacienteInfoPageState();

  const PacienteInfoPage({Key key}) : super(key: key);
}

class _PacienteInfoPageState extends State<PacienteInfoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SanAppBar(),
      body: Container(),
      bottomNavigationBar: PaciNavigationBar(2),
    );
  }
}
