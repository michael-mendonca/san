import 'package:SAN/components/linhaInfo.dart';
import 'package:SAN/models/alimento.dart' as ali;
import 'package:SAN/models/receita.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:flutter/services.dart';
import "package:intl/intl.dart";

class AdicionarReceitaPage extends StatefulWidget {
  @override
  _AdicionarReceitaPageState createState() => _AdicionarReceitaPageState();
}

class _AdicionarReceitaPageState extends State<AdicionarReceitaPage> {
  @override
  Widget build(BuildContext context) {
    final Receita receita = ModalRoute.of(context).settings.arguments;

    var f = new NumberFormat("###.##", "pt_BR");

    receita.pivotRefeicao = PivotRefeicao(porcoes: 1);

    return Scaffold(
        appBar: SanAppBar(titulo: 'Adicionar Receita'),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(receita.nome,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Container(
                        height: 50,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Quantidade de porções',
                                style: TextStyle(
                                    color: Color.fromRGBO(125, 125, 125, 1.0),
                                    fontSize: 16)),
                            Container(
                              width: 100,
                              height: 30,
                              child: TextField(
                                onChanged: (texto) {
                                  // print('Texto: $texto');
                                  receita.pivotRefeicao.porcoes =
                                      int.parse(texto);
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    filled: true,
                                    fillColor: Colors.white,
                                    focusColor: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 50,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Porções por preparo:',
                                style: TextStyle(
                                    color: Color.fromRGBO(125, 125, 125, 1.0),
                                    fontSize: 16)),
                            Text('${receita.porcoes} porções',
                                style: TextStyle(
                                    color: Color.fromRGBO(125, 125, 125, 1.0),
                                    fontSize: 16)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text('Modo de preparo:',
                      style: TextStyle(
                          color: Color.fromRGBO(125, 125, 125, 1.0),
                          fontSize: 16)),
                  Text(receita.preparo),
                ]),
                alimentosList(receita.alimentos),
                Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      LinhaInfo('Calorias',
                          f.format(receita.infoNutricionais.kcal), 'kcal'),
                      LinhaInfo('Proteínas',
                          f.format(receita.infoNutricionais.proteina), 'g'),
                      LinhaInfo('Lipídeos',
                          f.format(receita.infoNutricionais.lipideos), 'g'),
                      LinhaInfo('Colesterol',
                          f.format(receita.infoNutricionais.colesterol), 'mg'),
                      LinhaInfo('Carboidratos',
                          f.format(receita.infoNutricionais.carboidratos), 'g'),
                      LinhaInfo('Fibras',
                          f.format(receita.infoNutricionais.fibra), 'g'),
                      LinhaInfo('Cinzas',
                          f.format(receita.infoNutricionais.cinzas), 'g'),
                      LinhaInfo('Calcio',
                          f.format(receita.infoNutricionais.calcio), 'mg'),
                      LinhaInfo('Mangenésio',
                          f.format(receita.infoNutricionais.magnesio), 'mg'),
                      LinhaInfo('Manganes',
                          f.format(receita.infoNutricionais.manganes), 'mg'),
                      LinhaInfo('Fosforo',
                          f.format(receita.infoNutricionais.fosforo), 'mg'),
                      LinhaInfo('Ferro',
                          f.format(receita.infoNutricionais.ferro), 'mg'),
                      LinhaInfo('Sódio',
                          f.format(receita.infoNutricionais.sodio), 'mg'),
                      LinhaInfo('Potássio',
                          f.format(receita.infoNutricionais.potassio), 'mg'),
                      LinhaInfo('Cobre',
                          f.format(receita.infoNutricionais.cobre), 'mg'),
                      LinhaInfo('Zinco',
                          f.format(receita.infoNutricionais.zinco), 'mg'),
                      Container(height: 80),
                    ])
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () {
            print(
                "Retornando receita(${receita.nome} - ${receita.pivotRefeicao.porcoes})...");

            Navigator.of(context).pop(receita);
          },
        ));
  }
}

alimentosList(List<ali.Alimento> alimentos) {
  var f = new NumberFormat("###.##", "pt_BR");

  return ListView.builder(
    itemCount: alimentos.length,
    itemBuilder: (context, index) {
      return ListTile(
        title: Text(alimentos[index].nome),
        subtitle: Text(
            '${alimentos[index].detalhe}, ${alimentos[index].quantidade * alimentos[index].pivotReceita.porcoes}${alimentos[index].unidade.nome}'),
        trailing: Text(
            '${f.format(alimentos[index].kcal * alimentos[index].pivotReceita.porcoes)}kcal'),
      );
    },
    shrinkWrap: true,
    physics: ClampingScrollPhysics(),
  );
}
