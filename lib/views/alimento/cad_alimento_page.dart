import 'package:SAN/models/alimento.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import 'package:flutter/services.dart';
import "package:intl/intl.dart";

class AlimentoCadastroPage extends StatefulWidget {
  @override
  _AlimentoCadastroPageState createState() => _AlimentoCadastroPageState();
}

class _AlimentoCadastroPageState extends State<AlimentoCadastroPage> {
  Alimento alimento = Alimento.empty();
  int dropDownValue;

  @override
  Widget build(BuildContext context) {
    var f = new NumberFormat("###.##", "pt_BR");

    dropDownValue = 1;

    alimento.nome = 'Nome';
    alimento.unidade = Unidade.empty();

    return Scaffold(
      appBar: SanAppBar(titulo: 'Cadastrar Alimento'),
      body: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(bottom: 100),
            child: Column(
              children: [
                // inputLineString("Nome", "Obrigatório", alimento.nome),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Nome",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.nome = text;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Obrigatório",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Detalhe",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.detalhe = text;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Quantidade",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.quantidade = num.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Obrigatório",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      ),
                      FutureBuilder<List>(
                        future: API().listUnidades(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            // alimento.unidade = snapshot.data[0];
                            //dropDownValue = snapshot.data[0].id;

                            return DropdownButton(
                              value: dropDownValue,
                              onChanged: (newValue) {
                                dropDownValue = newValue;
                                alimento.unidade.id = newValue;

                                print(alimento.unidade.id);
                              },
                              items: snapshot.data.map((data) {
                                return DropdownMenuItem<int>(
                                    child: new Text(data.nome), value: data.id);
                              }).toList(),
                            );
                          } else {
                            return Text('Carregando...');
                          }
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Calorias (kcal)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.kcal = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Obrigatório",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Proteínas (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.proteina = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Lipídeos (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.lipideos = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Colesterol (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.colesterol = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Carboidratos (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.carboidratos = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Fibras (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.fibra = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Cinzas (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.cinzas = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Calcio (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.calcio = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Magnésio (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.kcal = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Magnésio (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.magnesio = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Manganês (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.manganes = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Fósforo (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.fosforo = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Ferro (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.ferro = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Sódio (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.sodio = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Potássio (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.potassio = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Cobre (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.cobre = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "     " + "Zinco (g)",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      SizedBox(
                        width: 100,
                        child: TextField(
                          onChanged: (text) {
                            alimento.zinco = double.parse(text);
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]+'))
                          ],
                          textInputAction: TextInputAction.next,
                          decoration: new InputDecoration(
                            labelText: "Opicional",
                            border: InputBorder.none,
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () async {
            print("Enviando: " + alimento.toJson().toString());
            Alimento alimentoRetorno = await API().createAlimento(alimento);

            print("Alimento cadastrado: " + alimentoRetorno.nome);
            Navigator.of(context)
                .pushNamed('/adicionarAlimento', arguments: alimentoRetorno)
                .then((alimentoRetorno) {
              if (alimentoRetorno is Alimento) {
                // print('Retorno final de alimento(${result.nome})...');
                Navigator.of(context).pop(alimentoRetorno);
              } else {
                print("Alimento não reconhecido - busca(adicionar)");
              }
            });
          }),
    );
  }

  // FutureBuilder<List> seletorUnidades() {
  //   return FutureBuilder<List>(
  //     future: API().listUnidades(),
  //     builder: (context, snapshot) {
  //       if (snapshot.hasData) {
  //         return DropdownButton(
  //             value: alimento.unidade,
  //             items: snapshot.data.map((unidade) {
  //               return DropdownMenuItem(
  //                   child: new Text(unidade.nome), value: unidade);
  //             }).toList(),
  //             onChanged: (newValue) {
  //               setState(() {
  //                 alimento.unidade = newValue;
  //               });
  //             });
  //       }
  //     },
  //   );
  // }
}
