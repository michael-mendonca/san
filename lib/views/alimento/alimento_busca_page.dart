import 'package:SAN/models/alimento.dart';
import 'package:SAN/models/paciente.dart';
import 'package:SAN/models/receita.dart';
import 'package:flutter/material.dart';
import 'package:SAN/models/dieta.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import 'package:flutter_icons/flutter_icons.dart';
import "package:intl/intl.dart";

import 'package:SAN/components/dietaList.dart';

class AlimentoBuscaPage extends StatefulWidget {
  @override
  _AlimentoBuscaPageState createState() => _AlimentoBuscaPageState();
}

class _AlimentoBuscaPageState extends State<AlimentoBuscaPage> {
  bool buscarReceitas = false;
  String busca = "";
  bool buscaRealizada = false;

  List<dynamic> resultList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: SanAppBar(titulo: 'Buscar Alimento'),
        body: Container(
          child: Column(
            children: [
              Container(
                height: 110,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextField(
                        onSubmitted: (text) {
                          busca = text;
                          buscaRealizada = true;
                          setState(() {});
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.search),
                            labelText: 'Buscar Alimento ou Receita',
                            border: OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.white,
                            focusColor: Colors.white),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Resultados da busca:',
                            style: TextStyle(
                                fontSize: 18,
                                color: Color.fromRGBO(125, 125, 125, 1.0)),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              FutureBuilder(
                  future: API().buscaAlimentos(busca),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.data == null) {
                      return Container(
                          child: Center(
                        child: Text("Carregando..."),
                      ));
                    } else {
                      resultList = snapshot.data;

                      return resultListView(resultList);
                    }
                  })
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).pushNamed('/createAlimento').then((result) {
              if (result is Alimento) {
                print('Retorno final de alimento(${result.nome})...');
                Navigator.of(context).pop(result);
              } else {
                print("Alimento não reconhecido - busca(create)");
              }
            });
          },
        ));
  }

  resultListView(List<dynamic> alimentos) {
    var f = new NumberFormat("###.##", "pt_BR");

    if (alimentos.isNotEmpty) {
      return Expanded(
        child: ListView.builder(
          itemCount: alimentos.length,
          itemBuilder: (context, index) {
            if (alimentos[index].runtimeType == Alimento) {
              return Padding(
                padding: const EdgeInsets.all(5.0),
                child: ListTile(
                  leading: Icon(FontAwesome5Solid.apple_alt, size: 30),
                  tileColor: Colors.white,
                  title: Text(alimentos[index].nome),
                  subtitle: Text(
                      '${alimentos[index].detalhe}, ${alimentos[index].quantidade}'),
                  trailing: Text('${f.format(alimentos[index].kcal)}kcal'),
                  onTap: () {
                    // print('Alimento: ${alimentos[index].nome}');
                    Navigator.of(context)
                        .pushNamed('/adicionarAlimento',
                            arguments: alimentos[index])
                        .then((result) {
                      if (result is Alimento) {
                        // print('Retorno final de alimento(${result.nome})...');
                        Navigator.of(context).pop(result);
                      } else {
                        print("Alimento não reconhecido - busca(adicionar)");
                      }
                    });
                  },
                ),
              );
            } else {
              return Padding(
                padding: const EdgeInsets.all(5.0),
                child: ListTile(
                  leading: Icon(Icons.menu_book, size: 30),
                  tileColor: Colors.white,
                  title: Text(alimentos[index].nome),
                  subtitle: Text(
                      '${alimentos[index].porcoes} ${alimentos[index].porcoes > 1 ? "porções" : "porção"}'),
                  trailing: Text(
                      '${f.format(alimentos[index].infoNutricionais.kcal)}kcal'),
                  onTap: () {
                    // print('Alimento: ${alimentos[index].nome}');
                    Navigator.of(context)
                        .pushNamed('/adicionarReceita',
                            arguments: alimentos[index])
                        .then((result) {
                      if (result is Receita) {
                        // print('Retorno final de alimento(${result.nome})...');
                        Navigator.of(context).pop(result);
                      } else {
                        print("Receita não reconhecida");
                      }
                    });
                  },
                ),
              );
            }
          },
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
        ),
      );
    } else if (buscaRealizada) {
      return Text("Alimento não encontrado");
    } else {
      return Text("Pesquisar alimentos");
    }
  }
}
