import 'package:SAN/components/LinhaInfo.dart';
import 'package:SAN/models/alimento.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:flutter/services.dart';
import "package:intl/intl.dart";

class AdicionarAlimentoPage extends StatefulWidget {
  @override
  _AdicionarAlimentoPageState createState() => _AdicionarAlimentoPageState();
}

class _AdicionarAlimentoPageState extends State<AdicionarAlimentoPage> {
  @override
  Widget build(BuildContext context) {
    final Alimento alimento = ModalRoute.of(context).settings.arguments;

    alimento.pivotRefeicao = PivotRefeicao(porcoes: 1);

    var f = new NumberFormat("###.##", "pt_BR");

    return Scaffold(
        appBar: SanAppBar(titulo: "Adicionar alimento"),
        body: Container(
          child: SingleChildScrollView(
            child: IntrinsicHeight(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text('${alimento.nome}, ${alimento.detalhe}',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: [
                        Container(
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Quantidade de porções',
                                  style: TextStyle(
                                      color: Color.fromRGBO(125, 125, 125, 1.0),
                                      fontSize: 16)),
                              Container(
                                width: 100,
                                height: 30,
                                child: TextField(
                                  onChanged: (texto) {
                                    setState(() {
                                      alimento.pivotRefeicao.porcoes =
                                          int.parse(texto);
                                    });

                                    print("Porções: " +
                                        alimento.pivotRefeicao.porcoes
                                            .toString());
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusColor: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Tamanho da porção',
                                  style: TextStyle(
                                      color: Color.fromRGBO(125, 125, 125, 1.0),
                                      fontSize: 16)),
                              Text(
                                  '${alimento.quantidade}${alimento.unidade.nome}',
                                  style: TextStyle(
                                      color: Color.fromRGBO(125, 125, 125, 1.0),
                                      fontSize: 16)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        LinhaInfo('Calorias', f.format(alimento.kcal), 'kcal'),
                        LinhaInfo(
                            'Proteínas', f.format(alimento.proteina), 'g'),
                        LinhaInfo('Lipídeos', f.format(alimento.lipideos), 'g'),
                        LinhaInfo(
                            'Colesterol', f.format(alimento.colesterol), 'mg'),
                        LinhaInfo('Carboidratos',
                            f.format(alimento.carboidratos), 'g'),
                        LinhaInfo('Fibras', f.format(alimento.fibra), 'g'),
                        LinhaInfo('Cinzas', f.format(alimento.cinzas), 'g'),
                        LinhaInfo('Calcio', f.format(alimento.calcio), 'mg'),
                        LinhaInfo(
                            'Mangenésio', f.format(alimento.magnesio), 'mg'),
                        LinhaInfo(
                            'Manganes', f.format(alimento.manganes), 'mg'),
                        LinhaInfo('Fosforo', f.format(alimento.fosforo), 'mg'),
                        LinhaInfo('Ferro', f.format(alimento.ferro), 'mg'),
                        LinhaInfo('Sódio', f.format(alimento.sodio), 'mg'),
                        LinhaInfo(
                            'Potássio', f.format(alimento.potassio), 'mg'),
                        LinhaInfo('Cobre', f.format(alimento.cobre), 'mg'),
                        LinhaInfo('Zinco', f.format(alimento.zinco), 'mg'),
                        Container(height: 80)
                      ])
                ],
              ),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () {
            print(
                "Retornando alimento(${alimento.nome} - ${alimento.pivotRefeicao.porcoes})...");

            Navigator.of(context).pop(alimento);
          },
        ));
  }
}
