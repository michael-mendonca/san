import 'package:SAN/components/nutriNavigationBar.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';

class NutriPacientesPage extends StatefulWidget {
  @override
  _NutriPacientesPageState createState() => _NutriPacientesPageState();
}

class _NutriPacientesPageState extends State<NutriPacientesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SanAppBar(titulo: 'Pacientes'),
      body: Container(
          child: FutureBuilder(
              future: API().getPacientes(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                      child: Center(
                    child: Text("Carregando..."),
                  ));
                } else {
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                          tileColor: Colors.white,
                          leading: Icon(
                            Icons.account_circle_rounded,
                            size: 50,
                          ),
                          title: Text(snapshot.data[index].usuario.nome),
                          subtitle: Text(snapshot.data[index].usuario.email),
                          trailing: Icon(Icons.keyboard_arrow_right),
                          onTap: () {
                            Navigator.of(context).pushNamed('/pacienteMenu',
                                arguments: snapshot.data[index]);
                          });
                    },
                  );
                }
              })),
      bottomNavigationBar: NutriNavigationBar(1),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).pushNamed('/pacienteInfo');
        },
      ),
    );
  }
}
