import 'package:SAN/components/pacienteCard.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import 'package:intl/intl.dart';

class NutriPacienteDietasPage extends StatefulWidget {
  @override
  _NutriPacienteDietasPageState createState() =>
      _NutriPacienteDietasPageState();
}

class _NutriPacienteDietasPageState extends State<NutriPacienteDietasPage> {
  @override
  Widget build(BuildContext context) {
    final Paciente paciente = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: SanAppBar(titulo: 'Dietas do paciente'),
      body: Container(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: PacienteCard(paciente),
          ),
          Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Dietas:',
                    style: TextStyle(
                        fontSize: 20,
                        color: Color.fromRGBO(125, 125, 125, 1.0)),
                  ),
                ],
              )),
          Expanded(
            child: FutureBuilder(
                future: API().getDietasPaciente(paciente.id),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                        child: Center(
                      child: Text("Carregando..."),
                    ));
                  } else {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                            tileColor: Colors.white,
                            leading: Icon(
                              Icons.list,
                              size: 50,
                            ),
                            title: Text(snapshot.data[index].descricao),
                            subtitle: Text(DateFormat('dd/MM/yyyy').format(
                                DateTime.parse(
                                    snapshot.data[index].createdAt))),
                            trailing: Icon(Icons.keyboard_arrow_right),
                            onTap: () {
                              Navigator.of(context).pushNamed('/dietaInfo',
                                  arguments: snapshot.data[index].id);
                            });
                      },
                    );
                  }
                }),
          ),
        ],
      )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .pushNamed('/dietaCadastro', arguments: paciente)
              .then((_) => setState(() {}));
        },
      ),
    );
  }
}
