import 'package:SAN/components/nutriNavigationBar.dart';
import 'package:SAN/components/nutricionistaCard.dart';
import 'package:SAN/models/nutricionista.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';

class NutriHomePage extends StatefulWidget {
  @override
  _NutriHomePageState createState() => _NutriHomePageState();
}

class _NutriHomePageState extends State<NutriHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SanAppBar(titulo: 'Início'),
      body: Container(
          child: FutureBuilder(
              future: API().nutricionistaLogado(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                      child: Center(
                    child: Text("Carregando..."),
                  ));
                } else {
                  return NutricionistaCard(
                    nutricionista: snapshot.data,
                  );
                }
              })
          // NutricionistaCard(),
          ),
      bottomNavigationBar: NutriNavigationBar(0),
    );
  }
}
