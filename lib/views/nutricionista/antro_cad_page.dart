import 'package:SAN/components/numeroCard.dart';
import 'package:SAN/models/antropometria.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import "package:intl/intl.dart";

class AntropometriaCadastroPage extends StatefulWidget {
  @override
  _AntropometriaCadastroPageState createState() =>
      _AntropometriaCadastroPageState();
}

class _AntropometriaCadastroPageState extends State<AntropometriaCadastroPage> {
  Antropometria antro = Antropometria.empty();

  List<bool> selected = [true, false];

  @override
  Widget build(BuildContext context) {
    final Paciente paciente = ModalRoute.of(context).settings.arguments;

    antro.paciente = paciente;

    var f = new NumberFormat("###.##", "pt_BR");

    return Scaffold(
      appBar: SanAppBar(),
      body: Container(
        child: Column(
          children: [
            Text("Paciente: " + antro.paciente.usuario.nome),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Gênero: Masculino"),
                Text("   Idade: 25"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("IMC: " + f.format(antro.imc)),
                Text("   Gordura %: " + f.format(antro.gordura)),
                Text("   Peso Ideal: " + f.format(antro.pesoideal)),
              ],
            ),
            _buildCards(context),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        onPressed: () async {
          await API().createAntropometria(antro);
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _buildCards(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            // ToggleButtons(
            //     children: <Widget>[
            //       // Icon(FontAwesome.male),
            //       // Icon(FontAwesome.female),
            //       Text("Masculino"),
            //       Text("Feminino")
            //     ],
            //     onPressed: (int index) {
            //       setState(() {
            //         for (int buttonIndex = 0;
            //             buttonIndex < selected.length;
            //             buttonIndex++) {
            //           if (buttonIndex == index) {
            //             selected[buttonIndex] = !selected[buttonIndex];
            //           } else {
            //             selected[buttonIndex] = false;
            //           }
            //         }

            //         if (selected[0] == true) {
            //           antro.genero = 'masc';
            //         } else {
            //           antro.genero = 'fem';
            //         }

            //         print(antro.genero);
            //       });
            //     },
            //     isSelected: selected),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              NumeroCard(
                  title: "Peso",
                  subTitle: "(KG)",
                  initialValue: antro.peso,
                  minValue: 30,
                  maxValue: 200,
                  onChanged: (val) {
                    setState(() {
                      antro.peso = val;
                      antro.imc = antro.peso /
                          (antro.altura / 100 * antro.altura / 100);

                      if (antro.pcpeitoral != 0 &&
                          antro.pcab != 0 &&
                          antro.pccoxa != 0) {
                        int soma = antro.pcpeitoral + antro.pcab + antro.pccoxa;
                        double dc = 1.10938 -
                            (0.0008267 * soma) +
                            (0.0000016 * soma * soma) -
                            (0.0002574 * 25);
                        double g = (495 / dc) - 450;

                        double pi =
                            (antro.peso - (antro.peso * antro.gordura / 100)) /
                                0.75;

                        antro.gordura = g;
                        antro.pesoideal = pi;
                      }
                    });
                  }),
              NumeroCard(
                  title: "Altura",
                  subTitle: "(CM)",
                  initialValue: antro.altura,
                  minValue: 30,
                  maxValue: 200,
                  onChanged: (val) {
                    setState(() {
                      antro.altura = val;
                      antro.imc = antro.peso /
                          (antro.altura / 100 * antro.altura / 100);
                    });
                  }),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              NumeroCard(
                  title: "PCPEITORAL",
                  subTitle: "(MM)",
                  initialValue: antro.pcpeitoral,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pcpeitoral = val;

                      if (antro.pcpeitoral != 0 &&
                          antro.pcab != 0 &&
                          antro.pccoxa != 0) {
                        int soma = antro.pcpeitoral + antro.pcab + antro.pccoxa;
                        double dc = 1.10938 -
                            (0.0008267 * soma) +
                            (0.0000016 * soma * soma) -
                            (0.0002574 * 25);
                        double g = (495 / dc) - 450;

                        double pi =
                            (antro.peso - (antro.peso * antro.gordura / 100)) /
                                0.75;

                        antro.gordura = g;
                        antro.pesoideal = pi;
                      }
                    });
                  }),
              NumeroCard(
                  title: "PCAB",
                  subTitle: "(MM)",
                  initialValue: antro.pcab,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pcab = val;

                      if (antro.pcpeitoral != 0 &&
                          antro.pcab != 0 &&
                          antro.pccoxa != 0) {
                        int soma = antro.pcpeitoral + antro.pcab + antro.pccoxa;
                        double dc = 1.10938 -
                            (0.0008267 * soma) +
                            (0.0000016 * soma * soma) -
                            (0.0002574 * 25);
                        double g = (495 / dc) - 450;

                        double pi =
                            (antro.peso - (antro.peso * antro.gordura / 100)) /
                                0.75;

                        antro.gordura = g;
                        antro.pesoideal = pi;
                      }
                    });
                  }),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              NumeroCard(
                  title: "PCPCOXA",
                  subTitle: "(MM)",
                  initialValue: antro.pccoxa,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pccoxa = val;

                      if (antro.pcpeitoral != 0 &&
                          antro.pcab != 0 &&
                          antro.pccoxa != 0) {
                        int soma = antro.pcpeitoral + antro.pcab + antro.pccoxa;
                        double dc = 1.10938 -
                            (0.0008267 * soma) +
                            (0.0000016 * soma * soma) -
                            (0.0002574 * 25);
                        double g = (495 / dc) - 450;

                        double pi =
                            (antro.peso - (antro.peso * antro.gordura / 100)) /
                                0.75;

                        antro.gordura = g;
                        antro.pesoideal = pi;
                      }
                    });
                  }),
              NumeroCard(
                  title: "PCT",
                  subTitle: "(MM)",
                  initialValue: antro.pct,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pct = val;
                    });
                  }),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              NumeroCard(
                  title: "PCPCSI",
                  subTitle: "(MM)",
                  initialValue: antro.pcpcsi,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pcpcsi = val;
                    });
                  }),
              NumeroCard(
                  title: "PCB",
                  subTitle: "(MM)",
                  initialValue: antro.pcb,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pcb = val;
                    });
                  }),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              NumeroCard(
                  title: "PCSE",
                  subTitle: "(MM)",
                  initialValue: antro.pcse,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pcse = val;
                    });
                  }),
              NumeroCard(
                  title: "PCPANT",
                  subTitle: "(MM)",
                  initialValue: antro.pcpant,
                  minValue: 0,
                  maxValue: 500,
                  onChanged: (val) {
                    setState(() {
                      antro.pcpant = val;
                    });
                  }),
            ]),
          ],
        ),
      ),
    );
  }

  Widget _tempCard(String label) {
    return Card(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        child: Text(label),
      ),
    );
  }
}
