import 'package:SAN/components/cardTitle.dart';
import 'package:SAN/components/LinhaInfo.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import "package:intl/intl.dart";

class NutriPacienteAntropometriasPage extends StatefulWidget {
  @override
  _NutriPacienteAntropometriasPageState createState() =>
      _NutriPacienteAntropometriasPageState();
}

class _NutriPacienteAntropometriasPageState
    extends State<NutriPacienteAntropometriasPage> {
  @override
  Widget build(BuildContext context) {
    final Paciente paciente = ModalRoute.of(context).settings.arguments;

    var f = new NumberFormat("###.##", "pt_BR");

    return Scaffold(
      appBar: SanAppBar(titulo: 'Antropometria'),
      body: Container(
          child: FutureBuilder(
              future: API().getAntropometriasPaciente(paciente.id),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                      child: Center(
                    child: Text("Carregando..."),
                  ));
                } else {
                  // Text("Peso: ${f.format(snapshot.data[index].peso)}")
                  return new Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      return new Card(
                        margin: EdgeInsets.only(
                          left: 5,
                          right: 5,
                          top: 5,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            LinhaInfo(
                                'Data de registro',
                                DateFormat('dd/MM/yyyy').format(DateTime.parse(
                                    snapshot.data[index].createdAt)),
                                ''),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey.shade300))),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        "IMC: " +
                                            f.format(snapshot.data[index].imc),
                                        style:
                                            TextStyle(
                                                color: Color.fromRGBO(
                                                    125, 125, 125, 1.0),
                                                fontSize: 16)),
                                    Text(
                                        "   Gordura %: " +
                                            f.format(
                                                snapshot.data[index].gordura),
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                125, 125, 125, 1.0),
                                            fontSize: 16)),
                                    Text(
                                        "   Peso Ideal: " +
                                            f.format(
                                                snapshot.data[index].pesoideal),
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                125, 125, 125, 1.0),
                                            fontSize: 16)),
                                  ],
                                ),
                              ),
                            ),
                            Column(
                              children: [
                                LinhaInfo(
                                    'Peso',
                                    '${f.format(snapshot.data[index].peso)}',
                                    'kg'),
                                LinhaInfo(
                                    'Altura',
                                    '${f.format(snapshot.data[index].altura)}',
                                    'cm'),
                                LinhaInfo(
                                    'PCPeitoral',
                                    '${f.format(snapshot.data[index].pcpeitoral)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCT',
                                    '${f.format(snapshot.data[index].pct)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCAB',
                                    '${f.format(snapshot.data[index].pcab)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCSI',
                                    '${f.format(snapshot.data[index].pcpcsi)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCSE',
                                    '${f.format(snapshot.data[index].pcse)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCCOXA',
                                    '${f.format(snapshot.data[index].pccoxa)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCB',
                                    '${f.format(snapshot.data[index].pcb)}',
                                    'mm'),
                                LinhaInfo(
                                    'PCPANT',
                                    '${f.format(snapshot.data[index].pcpant)}',
                                    'mm'),
                              ],
                            ),
                            Container(
                              height: 50,
                            )
                          ],
                        ),
                      );
                    },
                    loop: false,
                    index: snapshot.data.length - 1,
                    itemCount: snapshot.data.length,
                    itemWidth: 300.0,
                    itemHeight: 400.0,
                    layout: SwiperLayout.DEFAULT,
                  );
                }
              })),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .pushNamed('/antropometriaCadastro', arguments: paciente)
              .then((_) => setState(() {}));
        },
      ),
    );
  }
}
