import 'package:SAN/components/nutriNavigationBar.dart';
import 'package:SAN/components/pacienteCard.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';

import 'package:SAN/components/sanAppBar.dart';

class PacienteMenuPage extends StatefulWidget {
  @override
  _PacienteMenuPageState createState() => _PacienteMenuPageState();
}

class _PacienteMenuPageState extends State<PacienteMenuPage> {
  @override
  Widget build(BuildContext context) {
    final Paciente paciente = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: SanAppBar(titulo: 'Menu Paciente'),
      body: Column(children: [
        PacienteCard(paciente),
        GridView.count(
          shrinkWrap: true,
          primary: true,
          crossAxisCount: 2,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(10.0),
                child: FlatButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed('/pacientePerfil', arguments: paciente);
                    },
                    color: Colors.green[200],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.person,
                          size: 60,
                        ),
                        Text(
                          "Perfil",
                          textScaleFactor: 2,
                        )
                      ],
                    ))),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/nutriPacienteDietas',
                          arguments: paciente);
                    },
                    color: Colors.green[200],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.assignment,
                          size: 60,
                        ),
                        Text(
                          "Dietas",
                          textScaleFactor: 2,
                        )
                      ],
                    ))),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                          '/nutriPacienteAntropometrias',
                          arguments: paciente);
                    },
                    color: Colors.green[200],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          IconData(0xeb5f, fontFamily: 'MaterialIcons'),
                          size: 60,
                        ),
                        Text(
                          "Antropometria",
                          textScaleFactor: 1.6,
                        )
                      ],
                    ))),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: FlatButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed('/pacienteConsumo', arguments: paciente);
                    },
                    color: Colors.green[200],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.food_bank,
                          size: 60,
                        ),
                        Text(
                          "Consumo",
                          textScaleFactor: 2,
                        )
                      ],
                    )))
          ],
        )
      ]),
      bottomNavigationBar: NutriNavigationBar(1),
    );
  }
}
