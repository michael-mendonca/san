import 'dart:math';

import 'package:SAN/components/nutriNavigationBar.dart';
import 'package:SAN/models/consumoNutriente.dart';
import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class PaciConsumoPage extends StatefulWidget {
  @override
  _PaciConsumoPageState createState() => _PaciConsumoPageState();
}

class _PaciConsumoPageState extends State<PaciConsumoPage> {
  List<charts.Series<ConsumoChart, String>> _seriesPieData;
  List<charts.Series<ConsumoChart, String>> _seriesPieData2;

  List<bool> selected = [true, false];

  _generateData() {
    var pieData = [
      new ConsumoChart(
          'Gorduras (g)', 153, charts.MaterialPalette.red.shadeDefault),
      new ConsumoChart(
          'Carboidratos (g)', 195, charts.MaterialPalette.green.shadeDefault),
      new ConsumoChart(
          'Proteínas (g)', 75, charts.MaterialPalette.blue.shadeDefault),
    ];

    _seriesPieData.add(charts.Series(
        data: pieData,
        domainFn: (ConsumoChart consumo, _) => consumo.nutriente,
        measureFn: (ConsumoChart consumo, _) => consumo.consumo,
        id: 'Consumo Médio',
        colorFn: (ConsumoChart consumo, _) => consumo.cor,
        labelAccessorFn: (ConsumoChart row, _) => '${row.consumo}'));
  }

  _generateData2() {
    var pieData = [
      new ConsumoChart(
          'Café da manhã (kcal)', 51, charts.MaterialPalette.cyan.shadeDefault),
      new ConsumoChart(
          'Almoço (kcal)', 195, charts.MaterialPalette.blue.shadeDefault),
      new ConsumoChart(
          'Lanche (kcal)', 100, charts.MaterialPalette.indigo.shadeDefault),
      new ConsumoChart(
          'Jantar (kcal)', 133, charts.MaterialPalette.teal.shadeDefault),
    ];

    _seriesPieData2.add(charts.Series(
        data: pieData,
        domainFn: (ConsumoChart consumo, _) => consumo.nutriente,
        measureFn: (ConsumoChart consumo, _) => consumo.consumo,
        id: 'Consumo Calorias',
        colorFn: (ConsumoChart consumo, _) => consumo.cor,
        labelAccessorFn: (ConsumoChart row, _) => '${row.consumo}'));
  }

  @override
  void initState() {
    super.initState();

    _seriesPieData = List<charts.Series<ConsumoChart, String>>();
    _seriesPieData2 = List<charts.Series<ConsumoChart, String>>();
    _generateData();
    _generateData2();
  }

  @override
  Widget build(BuildContext context) {
    final Paciente paciente = ModalRoute.of(context).settings.arguments;

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: new AppBar(
          title: Text("SAN App"),
          bottom: const TabBar(
            tabs: [
              Tab(text: "Calorias"),
              Tab(text: "Nutrientes"),
              Tab(text: "Macro"),
            ],
          ),
        ),
        body: Container(
          child: TabBarView(
            children: <Widget>[
              Column(
                children: [
                  _buildSeletor(context),
                  Expanded(
                      child: charts.PieChart(
                    _seriesPieData2,
                    animate: true,
                    animationDuration: Duration(seconds: 1),
                    defaultRenderer: new charts.ArcRendererConfig(
                        arcWidth: 80,
                        arcRendererDecorators: [
                          new charts.ArcLabelDecorator()
                        ]),
                    behaviors: [
                      new charts.DatumLegend(
                        outsideJustification:
                            charts.OutsideJustification.endDrawArea,
                        horizontalFirst: false,
                        desiredMaxRows: 4,
                        cellPadding:
                            new EdgeInsets.only(right: 4.0, bottom: 4.0),
                        entryTextStyle: charts.TextStyleSpec(
                            color: charts.MaterialPalette.purple.shadeDefault,
                            fontFamily: 'Georgia',
                            fontSize: 11),
                      )
                    ],
                  )),
                ],
              ),
              SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _buildSeletor(context),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          linha('Proteínas(g)',
                              (10 + Random().nextInt(300)).toString()),
                          linha('Lipídeos(g)',
                              (10 + Random().nextInt(200)).toString()),
                          linha('Colesterol(g)',
                              (10 + Random().nextInt(200)).toString()),
                          linha('Carboidratos(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Fibras(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Cinzas(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Calcio(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Mangenésio(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Manganes(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Fosforo(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Ferro(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Sódio(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Potássio(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Cobre(g)',
                              (10 + Random().nextInt(100)).toString()),
                          linha('Zinco(g)',
                              (10 + Random().nextInt(100)).toString()),
                        ]),
                  ],
                ),
              ),
              Column(
                children: [
                  _buildSeletor(context),
                  Expanded(
                      child: charts.PieChart(
                    _seriesPieData,
                    animate: true,
                    animationDuration: Duration(seconds: 1),
                    defaultRenderer: new charts.ArcRendererConfig(
                        arcWidth: 80,
                        arcRendererDecorators: [
                          new charts.ArcLabelDecorator()
                        ]),
                    behaviors: [
                      new charts.DatumLegend(
                        outsideJustification:
                            charts.OutsideJustification.endDrawArea,
                        horizontalFirst: false,
                        desiredMaxRows: 4,
                        cellPadding:
                            new EdgeInsets.only(right: 4.0, bottom: 4.0),
                        entryTextStyle: charts.TextStyleSpec(
                            color: charts.MaterialPalette.purple.shadeDefault,
                            fontFamily: 'Georgia',
                            fontSize: 11),
                      )
                    ],
                  )),
                ],
              ),
            ],
          ),
        ),
        bottomNavigationBar: NutriNavigationBar(1),
      ),
    );
  }

  Widget _buildSeletor(BuildContext context) {
    return ToggleButtons(
        children: <Widget>[Text("Diário"), Text("Semanal")],
        onPressed: (int index) {
          setState(() {
            for (int buttonIndex = 0;
                buttonIndex < selected.length;
                buttonIndex++) {
              if (buttonIndex == index) {
                selected[buttonIndex] = !selected[buttonIndex];
              } else {
                selected[buttonIndex] = false;
              }
            }
          });
        },
        isSelected: selected);
  }

  Widget linha(String titulo, String valor) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '' + titulo + ': ',
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
          Text(
            valor,
            style: const TextStyle(fontSize: 16),
          )
        ],
      ),
    );
  }
}
