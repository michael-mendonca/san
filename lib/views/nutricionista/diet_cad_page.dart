import 'package:SAN/components/pacienteCard.dart';
import 'package:SAN/models/alimento.dart';
import 'package:SAN/models/paciente.dart';
import 'package:SAN/models/planejamento.dart';
import 'package:SAN/models/receita.dart';
import 'package:flutter/material.dart';
import 'package:SAN/models/dieta.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import "package:intl/intl.dart";

class DietaCadastroPage extends StatefulWidget {
  @override
  _DietaCadastroPageState createState() => _DietaCadastroPageState();
}

class _DietaCadastroPageState extends State<DietaCadastroPage> {
  Dieta dieta = Dieta.empty();

  @override
  Widget build(BuildContext context) {
    final Paciente paciente = ModalRoute.of(context).settings.arguments;

    dieta.paciente = paciente;

    var f = new NumberFormat("###.##", "pt_BR");

    return Scaffold(
      appBar: SanAppBar(
        titulo: 'Cadastrar Dieta',
      ),
      body: Container(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: PacienteCard(paciente),
            ),
            Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Dietas:',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color.fromRGBO(125, 125, 125, 1.0)),
                    ),
                  ],
                )),
            Padding(
              padding: EdgeInsets.all(20),
              child: TextField(
                onChanged: (text) {
                  dieta.descricao = text;
                },
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelText: 'Descrição',
                    border: OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.white,
                    focusColor: Colors.white),
              ),
            ),
            Container(
              width: 150,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: Colors.grey.shade300)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Text(
                      "Calorias: ",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey.shade400),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          f.format(caloriasDieta(dieta.planejamentos)),
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        Text(" kcal"),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: dieta.planejamentos.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: Column(
                        children: [
                          Container(
                            color: Colors.green,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '${dieta.planejamentos[index].refeicao.categoria.nome}',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  ),
                                  RaisedButton(
                                      onPressed: () => adicionarAlimento(
                                          context, paciente, index),
                                      color: Colors.transparent,
                                      elevation: 0,
                                      child:
                                          Icon(Icons.add, color: Colors.white))
                                ],
                              ),
                            ),
                          ),
                          ListView.builder(
                            itemCount: dieta
                                .planejamentos[index].refeicao.alimentos.length,
                            itemBuilder: (context, index2) {
                              return Dismissible(
                                key: UniqueKey(),
                                child: ListTile(
                                  title: Text(dieta.planejamentos[index]
                                      .refeicao.alimentos[index2].nome),
                                  subtitle: Text(
                                      '${dieta.planejamentos[index].refeicao.alimentos[index2].detalhe}, ${dieta.planejamentos[index].refeicao.alimentos[index2].quantidade * dieta.planejamentos[index].refeicao.alimentos[index2].pivotRefeicao.porcoes}${dieta.planejamentos[index].refeicao.alimentos[index2].unidade.nome}'),
                                  trailing: Text(
                                      '${f.format(dieta.planejamentos[index].refeicao.alimentos[index2].kcal * dieta.planejamentos[index].refeicao.alimentos[index2].pivotRefeicao.porcoes)}kcal'),
                                ),
                                onDismissed: (direction) {
                                  setState(() {
                                    dieta
                                        .planejamentos[index].refeicao.alimentos
                                        .removeAt(index2);
                                  });

                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text("Alimento removido")));
                                },
                                background: Container(color: Colors.red),
                              );
                            },
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                          ),
                          ListView.builder(
                            itemCount: dieta
                                .planejamentos[index].refeicao.receitas.length,
                            itemBuilder: (context, index2) {
                              return Dismissible(
                                key: UniqueKey(),
                                child: ListTile(
                                  title: Text(dieta.planejamentos[index]
                                      .refeicao.receitas[index2].nome),
                                  subtitle: Text(
                                      '${dieta.planejamentos[index].refeicao.receitas[index2].descricao}, ${dieta.planejamentos[index].refeicao.receitas[index2].pivotRefeicao.porcoes} ${dieta.planejamentos[index].refeicao.receitas[index2].pivotRefeicao.porcoes > 1 ? "porções" : "porção"}'),
                                  trailing: Text(
                                      '${f.format(dieta.planejamentos[index].refeicao.receitas[index2].infoNutricionais.kcal * dieta.planejamentos[index].refeicao.receitas[index2].pivotRefeicao.porcoes)}kcal'),
                                ),
                                onDismissed: (direction) {
                                  setState(() {
                                    dieta.planejamentos[index].refeicao.receitas
                                        .removeAt(index2);
                                  });

                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text("Receita removida")));
                                },
                                background: Container(color: Colors.red),
                              );
                            },
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                          )
                        ],
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        onPressed: () async {
          await API().createDieta(dieta);
          Navigator.of(context).pop();
        },
      ),
    );
  }

  _pickTime(TimeOfDay time) async {
    TimeOfDay t = await showTimePicker(
      context: context,
      initialTime: time,
      initialEntryMode: TimePickerEntryMode.input,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );

    if (t != null) {
      setState(() {
        time = t;
      });
    }
  }

  num caloriasDieta(List<Planejamento> planejamentos) {
    num consumo = 0.00;
    if (planejamentos != null && planejamentos.isNotEmpty) {
      // print("Entrou em registros");
      planejamentos.forEach((plan) {
        if (plan.refeicao.alimentos != null &&
            plan.refeicao.alimentos.isNotEmpty) {
          plan.refeicao.alimentos.forEach((ali) {
            // print("Alimento: " + ali.nome);
            consumo += ali.kcal * ali.pivotRefeicao.porcoes;
          });
        }

        if (plan.refeicao.receitas != null &&
            plan.refeicao.receitas.isNotEmpty) {
          plan.refeicao.receitas.forEach((rec) {
            // print("Receita: " + rec.nome);
            consumo += rec.infoNutricionais.kcal * rec.pivotRefeicao.porcoes;
          });
        }
      });
    }
    // print(consumo.toString());

    return consumo;
  }

  adicionarAlimento(BuildContext context, Paciente paciente, int index) async {
    Navigator.of(context)
        .pushNamed('/alimentoBusca', arguments: paciente)
        .then((result) {
      if (result is Alimento) {
        print("Alimento ${result.nome} retornado");
        setState(() {
          dieta.planejamentos[index].refeicao.alimentos.add(result);
        });
      } else if (result is Receita) {
        print("Receita ${result.nome} retornada");
        setState(() {
          dieta.planejamentos[index].refeicao.receitas.add(result);
        });
      } else {
        print("Alimento não reconhecido");
      }
    });
/*    var alimento = await Navigator.push(
      context,
      new MaterialPageRoute(
          settings: RouteSettings(name: '/', arguments: paciente),
          builder: (BuildContext context) => AlimentoBuscaPage()),
    );
    //.pushNamed('/alimentoBusca', arguments: paciente);

  Scaffold.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text('${alimento.nome}')));
*/
  }
}
