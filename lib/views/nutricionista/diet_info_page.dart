import 'package:SAN/components/pacienteCard.dart';
import 'package:SAN/models/planejamento.dart';
import 'package:flutter/material.dart';
import 'package:SAN/models/dieta.dart';
import 'package:SAN/models/alimento.dart';
import 'package:SAN/components/sanAppBar.dart';
import 'package:SAN/api.dart';
import "package:intl/intl.dart";

class DietaInfoPage extends StatefulWidget {
  @override
  _DietaInfoPageState createState() => _DietaInfoPageState();
}

class _DietaInfoPageState extends State<DietaInfoPage> {
  @override
  Widget build(BuildContext context) {
    final int dietaId = ModalRoute.of(context).settings.arguments;
    var caloriasTotais;

    var f = new NumberFormat("###.##", "pt_BR");

    Dieta dieta;

    return Scaffold(
      appBar: SanAppBar(
        titulo: 'Detalhe Dieta',
      ),
      body: Container(
          child: FutureBuilder(
              future: API().getDieta(dietaId),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Container(
                      child: Center(
                    child: Text("Carregando..."),
                  ));
                } else {
                  dieta = snapshot.data;

                  print("Descricao: ");
                  print(snapshot.data.descricao);

                  return Column(
                    children: [
                      PacienteCard(snapshot.data.paciente),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                snapshot.data.descricao,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Color.fromRGBO(125, 125, 125, 1.0)),
                              ),
                            ],
                          )),
                      Container(
                        width: 150,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.grey.shade300)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text(
                                "Calorias: ",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey.shade400),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    f.format(
                                        caloriasDieta(dieta.planejamentos)),
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(" kcal"),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                            itemCount: dieta.planejamentos.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                child: Column(
                                  children: [
                                    Container(
                                      color: Colors.green,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${dieta.planejamentos[index].refeicao.categoria.nome}',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    ListView.builder(
                                      itemCount: dieta.planejamentos[index]
                                          .refeicao.alimentos.length,
                                      itemBuilder: (context, index2) {
                                        return ListTile(
                                          title: Text(dieta.planejamentos[index]
                                              .refeicao.alimentos[index2].nome),
                                          subtitle: Text(
                                              '${dieta.planejamentos[index].refeicao.alimentos[index2].detalhe}, ${dieta.planejamentos[index].refeicao.alimentos[index2].quantidade * dieta.planejamentos[index].refeicao.alimentos[index2].pivotRefeicao.porcoes}${dieta.planejamentos[index].refeicao.alimentos[index2].unidade.nome}'),
                                          trailing: Text(
                                              '${f.format(dieta.planejamentos[index].refeicao.alimentos[index2].kcal * dieta.planejamentos[index].refeicao.alimentos[index2].pivotRefeicao.porcoes)}kcal'),
                                        );
                                      },
                                      shrinkWrap: true,
                                      physics: ClampingScrollPhysics(),
                                    ),
                                    ListView.builder(
                                      itemCount: dieta.planejamentos[index]
                                          .refeicao.receitas.length,
                                      itemBuilder: (context, index2) {
                                        return ListTile(
                                          title: Text(dieta.planejamentos[index]
                                              .refeicao.receitas[index2].nome),
                                          subtitle: Text(
                                              '${dieta.planejamentos[index].refeicao.receitas[index2].descricao}, ${dieta.planejamentos[index].refeicao.receitas[index2].pivotRefeicao.porcoes} ${dieta.planejamentos[index].refeicao.receitas[index2].pivotRefeicao.porcoes > 1 ? "porções" : "porção"}'),
                                          trailing: Text(
                                              '${f.format(dieta.planejamentos[index].refeicao.receitas[index2].infoNutricionais.kcal * dieta.planejamentos[index].refeicao.receitas[index2].pivotRefeicao.porcoes)}kcal'),
                                        );
                                      },
                                      shrinkWrap: true,
                                      physics: ClampingScrollPhysics(),
                                    )
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  );
                }
              })),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.add),
      //   onPressed: () {
      //     setState(() {
      //       //API().addAlimentoRefeicao();
      //     });
      //   },
      // ),
    );
  }

  num caloriasDieta(List<Planejamento> planejamentos) {
    num consumo = 0.00;
    if (planejamentos != null && planejamentos.isNotEmpty) {
      // print("Entrou em registros");
      planejamentos.forEach((plan) {
        if (plan.refeicao.alimentos != null &&
            plan.refeicao.alimentos.isNotEmpty) {
          plan.refeicao.alimentos.forEach((ali) {
            // print("Alimento: " + ali.nome);
            consumo += ali.kcal * ali.pivotRefeicao.porcoes;
          });
        }

        if (plan.refeicao.receitas != null &&
            plan.refeicao.receitas.isNotEmpty) {
          plan.refeicao.receitas.forEach((rec) {
            // print("Receita: " + rec.nome);
            consumo += rec.infoNutricionais.kcal * rec.pivotRefeicao.porcoes;
          });
        }
      });
    }
    // print(consumo.toString());

    return consumo;
  }
}
