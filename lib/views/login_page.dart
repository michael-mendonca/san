import 'package:SAN/models/usuario.dart';
import 'package:flutter/material.dart';

import '../api.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String senha = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.green[800], Colors.green[500]]))),
      _body(),
    ]));
  }

  Widget _body() {
    return SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: EdgeInsets.all(50.0),
                    child: Image(image: AssetImage('assets/images/Logo.png'))),
                TextField(
                  onChanged: (text) {
                    email = text;
                  },
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(
                      labelText: 'Email',
                      border: OutlineInputBorder(),
                      filled: true,
                      fillColor: Colors.green.shade300,
                      focusColor: Colors.white),
                ),
                SizedBox(height: 10),
                TextField(
                  onChanged: (text) {
                    senha = text;
                  },
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.green.shade300,
                      labelText: 'Senha',
                      border: OutlineInputBorder()),
                ),
                SizedBox(height: 10),
                RaisedButton(
                    onPressed: () async {
                      login(email, senha);
                    },
                    child: Text('Entrar')),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Cadastre-se",
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                )
              ],
            )),
      ),
    );
  }

  login(String email, senha) async {
    Usuario usuario = await API().login(email, senha);

    if (usuario != null) {
      if (usuario.perfil == 1) {
        print('Login Nutricionista');
        Navigator.of(context).pushReplacementNamed('/nutriHome');
      } else if (usuario.perfil == 2) {
        print('Login Paciente');
        Navigator.of(context).pushReplacementNamed('/paciHome');
      }
    } else {
      print('Falha no login');
    }
  }
}
