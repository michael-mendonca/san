import 'package:SAN/components/cardTitle.dart';
import 'package:SAN/components/widgetUtils.dart' show screenAwareSize;
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class NumeroCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final int minValue;
  final int maxValue;
  final int initialValue;
  final ValueChanged<int> onChanged;

  const NumeroCard(
      {Key key,
      this.title,
      this.subTitle,
      this.minValue,
      this.maxValue,
      this.initialValue,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(
        left: screenAwareSize(5.0, context),
        right: screenAwareSize(5.0, context),
        top: screenAwareSize(5.0, context),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CardTitle(this.title, subtitle: this.subTitle),
          Container(
            decoration: BoxDecoration(),
            child: Row(
              children: [
                Icon(Icons.arrow_back_ios, color: Colors.grey[200], size: 20),
                NumberPicker.horizontal(
                    listViewHeight: 50,
                    initialValue: this.initialValue,
                    minValue: this.minValue,
                    maxValue: this.maxValue,
                    onChanged: (val) => onChanged(val)),
                Icon(Icons.arrow_forward_ios,
                    color: Colors.grey[200], size: 20),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
