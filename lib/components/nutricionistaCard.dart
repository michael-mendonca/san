import 'package:SAN/models/nutricionista.dart';
import 'package:flutter/material.dart';

class NutricionistaCard extends StatefulWidget {
  final Nutricionista nutricionista;

  NutricionistaCard({Key key, @required this.nutricionista}) : super(key: key);

  @override
  _NutricionistaCardState createState() => _NutricionistaCardState();
}

class _NutricionistaCardState extends State<NutricionistaCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        leading: Icon(Icons.person),
        title: Text(widget.nutricionista.usuario.nome),
        subtitle: Text(widget.nutricionista.usuario.email),
      ),
      Text('CRN: ' + widget.nutricionista.crn)
    ]));
  }
}
