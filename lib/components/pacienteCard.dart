import 'package:SAN/models/paciente.dart';
import 'package:flutter/material.dart';

class PacienteCard extends StatefulWidget {
  final Paciente paciente;

  PacienteCard(Paciente paciente) : this.paciente = paciente;

  @override
  _PacienteCardState createState() => _PacienteCardState(paciente);
}

class _PacienteCardState extends State<PacienteCard> {
  _PacienteCardState(this.paciente);
  final Paciente paciente;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        leading: Icon(Icons.person),
        title: Text(this.paciente.usuario.nome),
        subtitle: Text(this.paciente.usuario.email),
      )
    ]));
  }
}
