import 'package:flutter/material.dart';

class LinhaInfo extends StatefulWidget {
  final String titulo;
  final String valor;
  final String medida;

  const LinhaInfo(this.titulo, this.valor, this.medida);

  @override
  _LinhaInfoState createState() => _LinhaInfoState();
}

class _LinhaInfoState extends State<LinhaInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: Colors.grey.shade300))),
      child: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
        child: Container(
          height: 20,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                widget.titulo + ': ',
                style: const TextStyle(
                    color: Color.fromRGBO(125, 125, 125, 1.0), fontSize: 16),
              ),
              Text(
                widget.valor + " " + widget.medida,
                style: const TextStyle(
                    color: Color.fromRGBO(125, 125, 125, 1.0), fontSize: 18),
              )
            ],
          ),
        ),
      ),
    );
  }
}
