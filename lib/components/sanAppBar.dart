import 'package:flutter/material.dart';

class SanAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String titulo;

  SanAppBar({String titulo}) : this.titulo = titulo;

  @override
  _SanAppBarSate createState() => _SanAppBarSate(this.titulo);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _SanAppBarSate extends State<SanAppBar> {
  _SanAppBarSate(this.titulo);
  final String titulo;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(titulo == null ? "SAN App" : titulo),
    );
  }
}
