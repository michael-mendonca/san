import 'package:SAN/models/alimento.dart';
import 'package:flutter/cupertino.dart';
import "package:intl/intl.dart";

abstract class ListAlimentos {
  Widget buildTitle(BuildContext context);
  Widget buildSubtitle(BuildContext context);
  Widget buildTrailing(BuildContext context);
}

class AlimentoItem implements ListAlimentos {
  final Alimento alimento;

  AlimentoItem(this.alimento);

  var f = new NumberFormat("###.##", "pt_BR");

  @override
  Widget buildTitle(BuildContext context) => Text(alimento.nome);

  @override
  Widget buildSubtitle(BuildContext context) =>
      Text('${alimento.detalhe}, ${alimento.quantidade}');

  @override
  Widget buildTrailing(BuildContext context) =>
      Text('${f.format(alimento.kcal)}kcal');
}
