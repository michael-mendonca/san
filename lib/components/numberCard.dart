import 'package:SAN/components/cardTitle.dart';
import 'package:SAN/components/numberSlider.dart';
import 'package:SAN/components/widgetUtils.dart' show screenAwareSize;
import 'package:flutter/material.dart';

class NumberCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final int minValue;
  final int maxValue;
  final double startValue;
  final double value;
  final ValueChanged<double> onChanged;

  const NumberCard(
      {Key key,
      this.title,
      this.subTitle,
      this.minValue,
      this.maxValue,
      this.startValue,
      this.value = 0,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(
        left: screenAwareSize(16.0, context),
        right: screenAwareSize(4.0, context),
        top: screenAwareSize(4.0, context),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CardTitle(this.title, subtitle: this.subTitle),
          Expanded(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenAwareSize(16.0, context)),
                child: _drawSlider(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _drawSlider() {
    return WeightBackground(
      child: LayoutBuilder(
        builder: (context, constraints) {
          return constraints.isTight
              ? Container()
              : NumberSlider(
                  minValue: this.minValue,
                  maxValue: this.maxValue,
                  value: value,
                  onChanged: (val) => onChanged(val),
                  width: constraints.maxWidth,
                );
        },
      ),
    );
  }
}

class WeightBackground extends StatelessWidget {
  final Widget child;

  const WeightBackground({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Container(
          height: screenAwareSize(60.0, context),
          decoration: BoxDecoration(
            color: Color.fromRGBO(244, 244, 244, 1.0),
            borderRadius:
                new BorderRadius.circular(screenAwareSize(50.0, context)),
          ),
          child: child,
        ),
        Icon(Icons.arrow_drop_up),
      ],
    );
  }
}
