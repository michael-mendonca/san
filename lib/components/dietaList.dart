import 'package:SAN/models/alimento.dart';
import 'package:SAN/models/dieta.dart';
import 'package:flutter/material.dart';
import "package:intl/intl.dart";

import 'package:SAN/api.dart';

class DietaList extends StatefulWidget implements PreferredSizeWidget {
  final Dieta dieta;

  DietaList({Dieta dieta}) : this.dieta = dieta;

  @override
  _DietaListState createState() => _DietaListState(dieta);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _DietaListState extends State<DietaList> {
  _DietaListState(this.dieta);
  final Dieta dieta;

  Future<void> openDialog() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text("Selecione um alimento"),
            children: <Widget>[
              Container(
                child: DropDown(),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dieta.planejamentos.length,
      itemBuilder: (BuildContext context, int index1) {
        return Column(
          children: <Widget>[
            Row(
              children: [
                Text(
                  dieta.planejamentos[index1].refeicao.categoria.nome,
                ),
                FlatButton(
                  onPressed: () {
                    openDialog();
                    //API().addAlimentoRefeicao(dieta.planejamentos[index1].refeicao.id, "5");
                    /*
                    setState(() {
                      dieta.planejamentos[index1].refeicao.alimentos.add(
                          Alimento(
                              id: 10, nome: "bolo", kcal: 200, unidadeId: 2));
                    });
                    */
                  },
                  child: Icon(Icons.add),
                )
              ],
            ),
            alimentosList(dieta.planejamentos[index1].refeicao.alimentos)
          ],
        );
      },
    );
  }
}

alimentosList(List<Alimento> alimentos) {
  var f = new NumberFormat("###.##", "pt_BR");

  return ListView.builder(
    itemCount: alimentos.length,
    itemBuilder: (context, index) {
      return ListTile(
        title: Text(alimentos[index].nome),
        subtitle: Text(
            '${alimentos[index].detalhe}, ${alimentos[index].quantidade * alimentos[index].pivotRefeicao.porcoes}${alimentos[index].unidade.nome}'),
        trailing: Text(
            '${f.format(alimentos[index].kcal * alimentos[index].pivotRefeicao.porcoes)}kcal'),
      );
    },
    shrinkWrap: true,
    physics: ClampingScrollPhysics(),
  );
}

class DropDown extends StatefulWidget {
  @override
  DropDownState createState() => DropDownState();
}

class DropDownState extends State<DropDown> {
  Alimento _alimentoSelecionado;
  List<DropdownMenuItem<Alimento>> _dropdownItems;

  changeItem(Alimento alimentoSelecionado) {
    setState(() {
      print(alimentoSelecionado.nome);
      _alimentoSelecionado = alimentoSelecionado;
      print(_alimentoSelecionado.nome);
    });
  }

  List<DropdownMenuItem<Alimento>> buildItems(List alimentos) {
    List<DropdownMenuItem<Alimento>> items = List();

    for (Alimento alimento in alimentos) {
      items.add(DropdownMenuItem(
          value: alimento, child: Text("${alimento.nome} - ${alimento.kcal}")));
    }

    return items;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: API().listAlimentos(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return Container(
                child: Center(
              child: Text("Carregando..."),
            ));
          } else {
            _dropdownItems = buildItems(snapshot.data);
            _alimentoSelecionado = _dropdownItems[0].value;

            return Column(children: <Widget>[
              DropdownButton(
                value: _alimentoSelecionado,
                items: _dropdownItems,
                onChanged: changeItem,
              ),
            ]);
          }
        });
  }
}
