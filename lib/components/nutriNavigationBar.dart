import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NutriNavigationBar extends StatefulWidget implements PreferredSizeWidget {
  final int initSelectedIndex;

  NutriNavigationBar(int initSelectedIndex)
      : this.initSelectedIndex = initSelectedIndex;

  @override
  _NutriNavigationBarSate createState() =>
      _NutriNavigationBarSate(initSelectedIndex);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _NutriNavigationBarSate extends State<NutriNavigationBar> {
  _NutriNavigationBarSate(this.initSelectedIndex);
  final int initSelectedIndex;

  @override
  Widget build(BuildContext context) {
    int _selectedIndex = initSelectedIndex;

    void _onItemTapped(int index) async {
      setState(() {
        _selectedIndex = index;
      });

      if (_selectedIndex == 0) {
        Navigator.of(context).pushReplacementNamed('/nutriHome');
      } else if (_selectedIndex == 1) {
        Navigator.of(context).pushReplacementNamed('/nutriPacientes');
      } else if (_selectedIndex == 2) {
        logout();
      }
    }

    return BottomNavigationBar(
      backgroundColor: Colors.white,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Início',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people),
          label: 'Pacientes',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.login_sharp),
          label: 'Sair',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.green[800],
      onTap: _onItemTapped,
    );
  }

  logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();

    Navigator.of(context).pushReplacementNamed("/");
  }
}
