import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PaciNavigationBar extends StatefulWidget implements PreferredSizeWidget {
  final int initSelectedIndex;

  PaciNavigationBar(int initSelectedIndex)
      : this.initSelectedIndex = initSelectedIndex;

  @override
  _PaciNavigationBarSate createState() =>
      _PaciNavigationBarSate(initSelectedIndex);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _PaciNavigationBarSate extends State<PaciNavigationBar> {
  _PaciNavigationBarSate(this.initSelectedIndex);
  final int initSelectedIndex;

  @override
  Widget build(BuildContext context) {
    int _selectedIndex = this.initSelectedIndex;

    void _onItemTapped(int index) async {
      setState(() {
        _selectedIndex = index;
      });

      if (_selectedIndex == 0) {
        Navigator.of(context).pushReplacementNamed('/paciHome');
      } else if (_selectedIndex == 1) {
        Navigator.of(context).pushReplacementNamed('/nutriPacientes');
      } else if (_selectedIndex == 2) {
        Navigator.of(context).pushReplacementNamed('/pacienteDietas');
      } else if (_selectedIndex == 3) {
        logout();
      }
    }

    return BottomNavigationBar(
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Início',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people),
          label: 'Perfil',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.assignment),
          label: 'Dietas',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.login_sharp),
          label: 'Sair',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.green[800],
      onTap: _onItemTapped,
    );
  }

  logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();

    Navigator.of(context).pushReplacementNamed("/");
  }
}
