import 'package:SAN/models/dieta.dart';
import 'package:SAN/models/usuario.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:SAN/api.dart';

void main() {
  test('Deve retornar o usuário logado a partir do email e senha providas',
      () async {
    const String email = 'nutricionista@email.com';
    const String senha = '123456';

    Usuario usuario = await API().login(email, senha);

    expect(usuario.nome == 'Nutricionista', true);
  });

  test('Teste do teste', () {
    const idPaciente = 1;

    expect(idPaciente == 1, true);
  });
}
